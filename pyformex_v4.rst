..

pyFormex v4
-----------
pyFormex version 4 contains some important design changes.
The goals were:

- Make pyFormex ready for the future: Python >= 3.12, PyQt6/PySide6, NumPy2
- Provide a cleaner user environment in the style of NumPy.
  So the user can do::

    import pyformex as pf

  and access all basic and important functionality as attributes of pf.
- Avoid and discourage the use of * imports.
- Define a pyFormex core language with a stable public API.
  All the definitions in the pyformex module are guaranteed to be
  supported and changes to the public API will be slow and documented.
- Reduce the number of required imports in applications.
- Reduce the impact of moving definitions or modules to another place.
- Allow more changes to pyFormex without impacting the user.
- Add more integration between the different pyFormex components.

The details
-----------
To achieve the goals the main pyformex module has to import all the
relevant pyFormex modules and all the important function and class definitions.
The pyformex module (which we'll henceforth call ``pf``, referring to its alias)
is the first one imported on startup and because it is used for communicating
global data between the modules, it is also imported in most of the other
modules. Simply importing all the relevant definitions into ``pf`` would create
circular dependencies. Therefore, the imports are collected into a special
module and are only imported from there into ``pf`` after the other modules have
been loaded.

Because pyFormex can be run in GUI or nonGUI mode, we have to do this in
two separate steps. The ``pyformex.core`` module collects all the nonGUI
definitions. They are imported into ``pf`` at the end of basic startup,
before the GUI is started. The relevant GUI definitions are collected
in a module ``pyformex.gui.guicore``, and its contents is added to ``pf``
at the end of GUI startup.

The benefits:

- Avoid all '*' imports. This avoids name clashes and allows the use
  of linter tools (like flake8) to check application code for forgotten or unused
  imports and unused variables. For user applications it means no longer use
  ``from pyformex.script import *`` or ``from pyformex.gui.draw import *``.
- Avoid the need for lots of imports to get to all the important user
  oriented functionality. Therefore, all the important core definitions
  (GUI and nonGUI) are imported in the pyformex module.
  Thus, most everything is accessible by doing
  ``import pyformex as pf``. Clearly, we are following the NumPy style here,
  where everything is got with ``import numpy as np``. Henceforth we will
  use the prefered alias ``pf`` for the pyformex module.
- All user oriented core modules are imported into ``pf``. Moreover, all the
  important definitions from these modules are also directly imported in ``pf``.
  Thus, ``pf.mesh`` is the ``pyformex.mesh`` module and ``pf.Mesh`` is short
  for the ``pyformex.mesh.Mesh`` class. Other functions in that module,
  like rectangleWithHole, require the full form ``pf.mesh.rectangleWithHole``,
  but are still accessible without importing anything.
- The core module defines what is imported in ``pf``, and the gui.guicore module
  defines what is additionally imported in ``pf`` when the GUI is loaded. T
- Define a stable public API. This is to ensure stability of the pyFormex
  platform for the long term. Changes to the public API will then be limited,
  announced and gradual, with clear upgrade paths.
  For now, we define the pyFormex public API as everything in the ``pf``
  module if

  - the name does not start with an underscore, OR
  - the name is contained in the ``_public_`` attribute of either the
    ``pf.core`` or the ``pf.gui.guicore`` module.

  This definition may be refined later.

Alternatives
------------
We had initially explored (and for now rejected) an alternative where the
nonGUI and GUI core languages were kept separate. Then one should do::

  import pyformex as pf
  import pyformex.gui as pg

and then access all nonGUI stuff from ``pf`` and GUI things from ``pg``.
We finally ended up merging the two together, because we felt the
distinction ``pf`` or ``pg`` too confusing, and it needs an extra import
statement. If one wants to have the distinction it is still possible to
do ``import pyformex.gui.guicore as pg``.


Other changes
-------------
Project
.......
- In pre-v4 a Project meant a set of pyFormex data saved to a file.
  Now we have decoupled the Project concept from the saved project file.
  This allows for an increased use and importance of the Project class
- Projects can now be saved in several formats. Preferential save format is
  .pzf, though .pyf will continue to be supported.
  (Actually, objects that have no own .pzf format are saved in .pyf format
  inside the .pzf container.)
- A ProjectManager has been added to interactively manage and view the
  contents of a project. There is a lot of room for additional functionality.
  It can be started from the Project menu, or by clicking on the Project
  button in the status bar.
- Since saving Geometry is also preferentially done in .pzf format, the
  Project manager can directly be used to manage Geometry as well. This
  allows integration of the various geometry menus.

FE modules
..........
Have been moved to a subpackage ``pf.fe``

Progress
--------
In order to achieve the above goals, we had to shuffle around and change a lot
of code. But finally pyFormex 4.0-dev1 has become fairly stable and is nearly
fully functional (on Python 3.11, Qt5, NumPy < 2.0).
It has been merged in the master branch.

All doctest are passed, all examples run. Not all GUI functions can be
tested however. So clearly, we expect that some things may not function
properly. Please test and report.

Python 3.12 and 3.13 and NumPy 2.0 have been tested and work properly in
nogui mode. Making the GUI mode work with Python >= 3.12 will take a lot more
work, because it does not support Qt5 anymore and Qt6 forcees a whole new
way of using OpenGL. So we will probably release a 4.0 version soon (after
updating some basic documentation) and leave the Qt6 based GUI for later.


Upgrade path
------------
Other than the new Project and ProjectManager, the changes to the functionality
between pyFormex 3.x and pyFormex 4 are limited.
Most of your applications will have to be adapted though, especially if you
used ``from pyformex.gui.draw import *``.
The recommended way to use pyFormex is now to do
``import pyformex as pf`` and prefix every pyFormex symbol with ``pf.``.
Thus: Mesh becomes pf.Mesh, draw becomes pf.draw.
In the console you can do ``pf.publicKeys()`` to see all the definitions
in the current public API.
One thing I'm not yet sure about is the standard color names: should we use
pf.red or pf.color.red? For now, I have converted most of my code to use
the string version: ``draw(..., color='red')``.

As a convenience to quickly get legacy applications running, the
``pyformex.gui.draw`` module is still available (but deprecated).
So if you still use ``from pyformex.gui.draw import *``, most things should
work unaltered. **We recommend however to convert your scripts to the new
style as soon as possible.**
It will take a while to get used (I still occasionally type ``draw`` instead
of ``pf.draw``), but you'll benefit in the end. I have detected and fixed many
bugs thanks to the ability of using linter tools.
All the examples have been transformed, so you can have a look at these to see
the impact on the code.

Note that the ``pyformex`` module is automatically imported as ``pf`` when
you are running a script/app, so for small scripts you can just start without
any import. But adding the ``import pyformex as pf`` has the benefit of
linter tools being able to detect missing imports.


How you can (and should) help
-----------------------------
- Convert your applications, test them and see what is not working (correctly).
  Report malfunctions.
- Make suggestions for what needs to or could be added to the pyFormex public
  API.
- Discuss the changed concept of pyFormex and possible other changes or
  additions you would wish.


.. End
