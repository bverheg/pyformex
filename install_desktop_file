#!/bin/bash
##
##  This file is part of the pyFormex project.
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: http://pyformex.org
##  Project page:  https://savannah.nongnu.org/projects/pyformex/
##  Copyright (C) Benedict Verhegghe (benedict.verhegghe@ugent.be)
##  Distributed under the GNU General Public License version 3 or later.
##
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##

usage() {
    cat <<EOF
Usage: install_desktop_file [-a|-m|-i|-u] NAME

  Installs a pyFormex desktop file in an xdg-compatible desktop,
  to allow the user to start pyFormex from the desktop applications
   menu and/or from an application launcher icon on the desktop.

  NAME: basename of the .desktop and .icon files. The files
        NAME.desktop and NAME.png should exist.

  The options are mutually exclusive, with -a being the default.
  -a: Do both actions of the -m and -i options.
  -m: Install pyFormex in the desktop applications menu.
  -i: Install a pyFormex launcher icon on the desktop. Depending on
      the desktop in use, the user may need to activate it.
  -u: Uninstall the pyFormex desktop integration (both the menu entry
      and the desktop icon).
EOF
}

ECHO=          # set to 'echo' to do a dry run
MODE="mi"
case "$1" in
    -a ) MODE="mi"; shift;;
    -m|-i|-u ) MODE=${1:1}; shift;;
esac
[[ -z $1 || -n $2 ]] && { usage; exit 1; }

NAME=$1
echo "pyFormex desktop integration, MODE=$MODE, NAME=$NAME"

DESKTOP=$NAME.desktop
ICON=$NAME.png
[[ -f $DESKTOP ]] || {
    echo "ERROR: Desktop file $DESKTOP not found."
    exit 1
}
[[ -f $ICON ]] || {
    echo "Icon file $ICON not found."
    exit 1
}

if [[ $MODE == u ]]; then
    $ECHO xdg-icon-resource uninstall --size 64 $NAME
    $ECHO xdg-desktop-menu uninstall $DESKTOP
    $ECHO xdg-desktop-icon uninstall $DESKTOP
else
    $ECHO xdg-icon-resource install --size 64 $NAME.png $NAME
    [[ $MODE == *m* ]] && $ECHO xdg-desktop-menu install $NAME.desktop
    [[ $MODE == *i* ]] && $ECHO xdg-desktop-icon install $NAME.desktop
fi

# TODO: add uninstall to pyFormex --remove
# and remove links as well

# End
