"""Metadoc

Extract attributes from a docstring.

.. metadata
  :level: advanced
  :topics: FEA
  :techniques: color, persistence

That's it.
"""

import re
re_meta = re.compile(' +:([a-z]+): ([a-zA-Z0-9_, ]*)')
re_meta1 = re.compile('_([a-z]+) = ([a-zA-Z0-9_, \[\]\'"]+)')


def splitstring(s):
    """Split a string at commas and strip blanks"""
    return [i.strip() for i in s.strip(' ,').split(',')]


def metadoc(docstring):
    """Get metadata from a docstring"""
    meta = {}
    mode = 0
    name = None
    for line in docstring.split('\n'):
        if mode == 0:
            if name is None:
                name = line
            elif line.startswith('.. metadata'):
                mode = 1
        elif mode == 1:
            if len(line) == 0:
                break
            if m := re_meta.match(line):
                key, val = m.groups()
                if key in ['topics', 'techniques']:
                    val = splitstring(val)
                meta[key] = val
        meta.setdefault('name', name)
    return meta


def name():
    try:
        meta = metadoc(__doc__)
        if 'name' in meta:
            return meta['name']
        return pf.Path(__file__).stem
    except Exception as e:
        # print(e)
        return '<unknown>'


def metadoc_from_file(filename):
    """Find metadoc from source file"""
    meta = {}
    with open(filename, 'r') as fil:
        mode = 0
        for line in fil:
            if line.startswith('"""'):
                if mode > 0:
                    break
                else:
                    mode = 1
        for i in range(8):    # Look any further?
            line = next(fil)
            print(line.strip('\n'))
            if m := re_meta1.match(line):
                key, val = m.groups()
                val = eval(val)
                meta[key] = val
    return meta

default_values = {
    'level': 'normal',
    'topics': [],
    'techniques': []
}
keys_required = tuple(default_values)
keys_split = ('topics', 'techniques')


def val2str(val):
    """Docstring representation of a metadoc value"""
    if isinstance(val, str):
        return val
    elif isinstance(val, list):
        return ', '.join(val)
    raise ValueError("Value shoud be a str or a list of str")


def meta2doc(meta):
    """Format meta data for inclusion in a docstring"""
    s = "\n.. metadata\n"
    for key in meta:
        s += f"  :{key}: {val2str(meta[key])}\n"
    return s


def convert_meta(filename, outfilename):
    """Convert the metadata in a pyFormex script to new docstring format"""
    meta = {}
    with open(filename, 'r') as fil, open(outfilename, 'w') as out:
        mode = 0
        for line in fil:
            if line.startswith('"""'):
                if mode > 0:
                    break   # do not write the final """
                else:
                    mode = 1
            out.write(line)
        buffer = ''    # buffer for lines following the end of docstring
        for i in range(8):    # Look any further?
            line = next(fil)
            if m := re_meta1.match(line):
                key, val = m.groups()
                # print(f"{key=}: {val=}")
                val = eval(val)
                meta[key] = val
            else:
                buffer += line
        print("meta:", meta)
        print("buffer:", buffer)
        if meta:
            out.write(meta2doc(meta))
            out.write('"""\n')
            out.write(buffer)
            for line in fil:
                out.write(line)
        return meta

# filename = pf.Path('BezierSpline.py')
# if meta := metadoc(pf.getDocString(filename)):
#     print(meta)
#     print(f"Already converted: {filename.name}")

# convert_meta('BezierSpline.py', 'temp.out')
# exit()

def convert_examples():
    files = pf.cfg['examplesdir'].listTree(includefile=[r'[A-Z].*[.]py$'],
                                           excludefile=[r'.*_converted[.]py$'])
    for filename in files:
        if metadoc(pf.getDocString(filename)):
            print(f"Already converted: {filename.name}")
            continue
        print(f"Converting {filename}")
        outfilename = filename.without_suffix + '_converted.py'
        if convert_meta(filename, outfilename):
            ans = pf.ask(f"{filename.name}\n\nOverwrite old source with the converted?",
                         ["Yes", "No", "Delete", "Cancel"])
            if ans == 'Cancel':
                break
            elif ans == 'Yes':
                dest = outfilename.move(filename)
                print(f"{dest} has been converted")
            elif ans == 'Delete':
                outfilename.remove()
                print(f"Conversion has been deleted")
            else:
                print(f"Conversion kept on {outfilename}")
        else:
            outfilename.remove()
            print(f"{filename} could not be converted")

default_imports = """\
import numpy as np
import pyformex as pf
pg = pf
from pyformex import _I, _G, _C, _T
_name = pf.Path(__file__).stem
"""
remove_lines = default_imports.split('\n')[:-1] + [
    'import pyformex.gui.guicore as pg',
    'pg = pf.gui'
    'pg = pf.gui.guicore'
]

print(remove_lines)

def sanitize_example(filename):
    """Sanitize the imports of the example"""
    outfilename = pf.Path(filename).without_suffix + '_converted.py'
    with open(filename, 'r') as fil, open(outfilename, 'w') as out:
        mode = 0
        for line in fil:
            if line.startswith('"""'):
                mode += 1
                if mode == 2:
                    out.write(line)
                    out.write(default_imports)
                    continue
            if mode == 2 and (sline:=line.strip('\n')) in remove_lines:
                print(f"Removing: {sline}")
                continue
            out.write(line)
    return outfilename


def sanitize_examples():
    files = pf.cfg['examplesdir'].listTree(includefile=[r'[A-Z].*[.]py$'],
                                           excludefile=[r'.*_converted[.]py$'])
    ans = None
    for filename in files:
        outfilename = sanitize_example(filename)
        if ans != 'All':
            ans = pf.ask(f"{filename.name}\n\n"
                         "Overwrite old source with the converted?",
                     ["Yes", "No", "Delete", "Cancel", "All"])
        if ans == 'Cancel':
            break
        elif ans in ('Yes', 'All'):
            dest = outfilename.move(filename)
            print(f"{dest} has been converted")
        elif ans == 'Delete':
            outfilename.remove()
            print(f"Conversion has been deleted")
        else:
            print(f"Conversion kept on {outfilename}")


pf.chdir(pf.cfg['examplesdir'])
sanitize_examples()
