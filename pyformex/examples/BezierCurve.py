#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""BezierCurve

This example illustrates the use of Bernstein polynomials to evaluate points
on an n-th degree Bezier curve. See also Casteljau.

.. metadata
  :level: normal
  :topics: geometry, curve
  :techniques: pattern, bezier
"""
import numpy as np
import pyformex as pf
from pyformex import _I
_name = pf.Path(__file__).stem


def run():
    # predefined control point patterns
    predefined = ['514', '1234', '51414336', 'custom']

    res = pf.askItems([
        _I(name='pattern', choices=predefined),
        _I(name='custom', value=''),
    ])

    if not res:
        return

    s = res['pattern']
    if s == 'custom':
        s = res['custom']

    if not s.startswith('l:'):
        s = 'l:' + s
    C = pf.Formex(s).toCurve()

    pf.clear()
    pf.linewidth(2)
    pf.flat()

    pf.draw(C, bbox='auto', view='front')
    pf.draw(C.coords)
    pf.drawNumbers(C.coords)

    pf.setDrawOptions({'bbox': None})
    n = 100
    u = np.arange(n+1)*1.0/n
    P = pf.nurbs.pointsOnBezierCurve(C.coords, u)
    pf.draw(P)


if __name__ == '__draw__':
    run()

# End
