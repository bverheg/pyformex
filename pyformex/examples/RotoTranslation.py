#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""RotoTranslation

This example illustrates the use of transformCS() to return to an original
reference system after a number of affine transformations.

.. metadata
  :level: advanced
  :topics: geometry
  :techniques: transform
  :acknowledgement: gianluca
"""
import numpy as np
import pyformex as pf
_name = pf.Path(__file__).stem


def atExit():
    pf.delay(savewait)

def createScene(text=None, caged=True, color=None, move=0):
    """Create a scene of the story.

    The scene draws the horse (H), with the specified color number (0..7),
    caged or not, with the local axes (CS), and possibly a text.
    If move > 0, the horse moves before the scene is drawn.
    The horse and cage actors are returned.
    """
    global line, H, C, CS
    if move:
        H, C, CS = [i.rotate(30, 1).rotate(-10., 2).translate(
            [0., -move*0.1, 0.]) for i in [H, C, CS]]

    if caged:
        cage = pf.draw(C, mode='wireframe', wait=False,)
    else:
        cage = None
    if color is None:
        color = 1 + np.random.randint(6)
    H.setProp(color)
    horse = pf.draw(H)
    if text:
        pf.drawText(text, (20, line), size=20)
        line += line_inc * len(text.split('\n'))
    pf.drawAxes(CS, size=0.5, psize=0.0)
    pf.zoomAll()
    pf.zoom(0.5)
    return horse, cage


def run():
    global line, line_inc, H, C, CS, savewait
    line_inc = -40
    line = pf.canvas.height() + line_inc
    savewait = pf.delay(0.5)
    pf.clear()
    pf.lights(True)
    pf.view('iso')
    pf.smooth()
    pf.transparent(state=False)
    pf.linewidth(2)
    pf.setDrawOptions({'bbox': None})

    # read the model of the horse
    F = pf.Formex.read(pf.cfg['datadir'] / 'horse.pgf')
    # make sure it is centered
    F = F.centered()
    # scale it to unity size and head it in the x-direction
    xmin, xmax = F.bbox()
    H = F.scale(1./(xmax[0]-xmin[0])).rotate(180, 1)
    # create the global coordinate system
    CS0 = CS = pf.CoordSys()
    # some text

    # A storage for the scenes
    script = []

    # Scene 0: The story starts idyllic
    T = 'There once was a white horse running free in the forest.'
    script += [createScene(text=T, caged=False, color=7)]
    pf.sleep(3)

    # Scene 1: Things turn out badly
    T = 'Some wicked pyFormex user caged the horse and transported it around.'
    # apply same transformations on model and coordinate system
    H, CS = [i.translate([0., 3., 6.]) for i in [H, CS]]
    C = pf.simple.cuboid(*H.bbox())
    script += [createScene(text=T)]
    pf.sleep(2)

    # Scene 2..n: caged movements
    T = 'The angry horse randomly changed colour at each step.'
    script += [createScene(text=T, move=1)]
    m = len(script)
    n = 16
    script += [createScene(move=i) for i in range(m, n, 1)]
    pf.sleep(2)

    # Scene n+1: the escape
    T = ('Finally the horse managed to escape from the cage.\n'
         'It wanted to go back home and turned black,\n'
         'so it would not be seen in the night.')
    escape = script[-1]
    script += [createScene(text=T, color=0, caged=False)]
    pf.undraw(escape)
    pf.sleep(3)

    # The problem
    T = "But alas, it couldn't remember how it got there!!!"
    pf.drawText(T, (20, line), size=20)
    line += line_inc
    for s in script[:-2]:
        pf.sleep(0.1)
        pf.undraw(s)
    pf.sleep(3)

    # The solution
    T = ("But thanks to pyFormex's orientation,\n"
         "it could go back in a single step,\n"
         "straight through the bushes.")
    pf.drawText(T, (20, line), size=20)
    line += 3*line_inc
    H = H.transformCS(CS0, CS)
    pf.draw(pf.Formex([[CS.points()[3], CS0.points()[3]]]))
    pf.sleep(3)

    T = "And the horse lived happily ever after."
    script += [createScene(text=T, color=7, caged=False)]
    pf.undraw(script[-2])


if __name__ == '__draw__':
    run()

# End
