#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""ObjectDialog

This example illustrates the interactive modification of object rendering.

The example creates the same geometry as the WebGL example, and then pops up
a dialog to allow the user to interactively change the rendering of the
objects. Attributes that can be changed include color, opacity, visibility,

.. metadata
  :level: normal
  :topics: export
  :techniques: webgl
"""
import pyformex as pf
from pyformex.opengl.objectdialog import objectDialog
from pyformex.examples.WebGL import createGeometry
_name = pf.Path(__file__).stem


def run():
    pf.reset()
    pf.clear()
    pf.smooth()
    pf.transparent()
    pf.bgcolor('white')
    pf.view('right')

    # Create some geometrical objects
    objects = createGeometry()

    # make them available in the GUI
    pf.PF.update([(obj.attrib.name, obj) for obj in objects])

    # draw the objects
    drawn_objects = pf.draw(objects)
    pf.zoomAll()

    # create an object dialog
    dialog = objectDialog(drawn_objects)
    if dialog:
        dialog.show()


if __name__ == '__draw__':
    run()

# End
