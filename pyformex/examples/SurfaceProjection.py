#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""SurfaceProjection.py

This example illustrates the use of Coords.projectOnSurface and
trisurface.intersectSurfaceWithLines as a method to render a 2D image
onto a 3D surface.

.. metadata
  :level: normal
  :topics: surface
  :techniques: transform, projection, dialog, image, isopar
"""
import pyformex as pf
from pyformex import _I
_name = pf.Path(__file__).stem


# def loadImage(fn):
#     global image, scaled_image
#     image = QImage(fn)
#     if image.isNull():
#         pf.warning("Could not load image '%s'" % fn)
#         return None
#     return image


def makeGrid(nx, ny, eltype):
    """Create a 2D grid of nx*ny elements of type eltype.

    The grid is scaled to unit size and centered.
    """
    return eltype.toFormex().replicm((nx, ny)).resized(1.).centered()


def drawImage(grid, base, patch, pcolor):
    """Draw the image on the specified patch grid.

    The image colors are specified in the global variable pcolor.
    grid is a Formex with px*py Quad8 elements.
    Each element of grid will be filled by a kx*ky patch of colors.
    """
    mT = [patch.isopar('quad8', x, base) for x in grid.coords]
    return [pf.draw(i, color=c, alpha=0.99, bbox='last', nolight=True,
                    wait=False) for i, c in zip(mT, pcolor)]


def run():
    global viewer
    pf.clear()
    pf.smooth()
    pf.lights(True)
    pf.transparent(False)
    pf.view('iso')

    # read the teapot surface
    T = pf.TriSurface.read(pf.cfg['datadir'] / 'teapot1.off')
    xmin, xmax = T.bbox()
    T = T.trl(-T.center()).scale(4./(xmax[0]-xmin[0])).setProp(2)
    pf.draw(T)

    # default image file
    filename = pf.cfg['datadir'] / 'benedict_6.jpg'
    viewer = pf.ImageView(filename, maxheight=200)
    res = pf.askItems([
        _I('filename', filename, text='Image file', itemtype='filename',
           filter='img', mode='exist', preview=viewer),
        viewer,
        _I('px', 4, text='Number of patches in x-direction'),
        _I('py', 6, text='Number of patches in y-direction'),
        _I('kx', 30, text='Width of a patch in pixels'),
        _I('ky', 30, text='Height of a patch in pixels'),
        _I('scale', 0.8, text='Scale factor'),
        _I('trl', [-0.4, -0.1, 2.], itemtype='point', text='Translation'),
    ])
    if res:
        project_image(T, **res)


def project_image(T, filename, px, py, kx, ky, scale, trl):
    nx, ny = px*kx, py*ky  # pixels
    print(f'The image is reconstructed with {nx} x {ny} pixels')
    print("Loading image")
    image = pf.Image.open(filename)
    wpic, hpic = image.size
    print(f"Image size is {wpic}x{hpic}")

    # Create the colors
    color = pf.image2array(image, size=(nx, ny), gl=True)
    # Reorder by patch
    pcolor = color.reshape((py, ky, px, kx, 3)).swapaxes(1, 2).reshape(-1, kx*ky, 3)
    print("Shape of the colors array: %s" % str(pcolor.shape))
    mH = makeGrid(px, py, pf.elements.Quad8)
    try:
        ratioYX = float(hpic)/wpic
        mH = mH.scale(ratioYX, 1)  # Keep original aspect ratio
    except Exception:
        pass
    mH0 = mH.scale(scale).translate(trl)

    dg0 = pf.draw(mH0, mode='wireframe')
    pf.zoomAll()
    pf.zoom(0.5)
    print("Create %s x %s patches" % (px, py))

    # Create the transforms
    base = makeGrid(1, 1, pf.elements.Quad8).coords[0]
    patch = makeGrid(kx, ky, pf.elements.Quad4).toMesh()
    d0 = drawImage(mH0, base, patch, pcolor)

    # Direction of projection
    v = [0., 0., 1.]
    pts, ind = mH0.coords.projectOnSurface(T, v, missing='r', return_indices=True)
    if pts.npoints() < mH0.coords.npoints():
        # translate the points that miss the surface in projection
        # over the maximum distance of projected points (in -z direction)
        x = mH0.coords.reshape(-1, 3)
        d = pf.at.length(x[ind] - pts)
        x = x.trl(v, -d.max())
        # fill in the projected points
        x[ind] = pts
        pts = x

    dp = pf.draw(pts, marksize=6, color='white')
    mH2 = pf.Formex(pts.reshape(-1, 8, 3))
    x = pf.formex.connect([mH0.points(), mH2.points()])
    dx = pf.draw(x)

    print("Create projection mapping using the grid points")
    drawImage(mH2.trl([0., 0., 0.01]), base, patch, pcolor)
    # the small translation is to make sure the image is
    # above the surface, not cutting it

    print("Finally show the finished image")
    pf.undraw(dp)
    pf.undraw(dx)
    pf.undraw(d0)
    pf.undraw(dg0)
    pf.view('front')
    pf.zoomAll()


if __name__ == '__draw__':
    run()

# End
