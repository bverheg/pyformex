#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Double Layer Flat Space Truss Roof

.. metadata
  :level: advanced
  :topics: FEA
  :techniques: color
"""
import numpy as np
import pyformex as pf
from pyformex import fe
from pyformex.examples.SpaceTrussRoof import createRoof
_name = pf.Path(__file__).stem


def createModel(dx, ht, nx, ny):
    """Create a finite elemnt model of a space truss roof."""

    # Create the geometry: See example SpaceTrussRoof
    M = createRoof(dx, ht, nx, ny).toMesh()

    # Show model
    pf.createView('myview1', (10., 15., 0.))
    pf.clear()
    pf.linewidth(1)
    pf.draw(M, view='myview1')

    # Create element sets
    topbar = np.where(M.prop==3)[0]
    bottombar = np.where(M.prop==0)[0]
    diabar = np.where(M.prop==1)[0]

    # Create node sets
    nnod = M.ncoords()
    nlist = np.arange(nnod)
    count = np.zeros(nnod)
    for n in M.elems.flat:
        count[n] += 1
    field = nlist[count==8]
    topedge = nlist[count==7]
    topcorner = nlist[count==6]
    bottomedge = nlist[count==5]
    bottomcorner = nlist[count==3]
    print(topedge)
    if ht > 0:
        topedge, bottomedge = bottomedge, topedge
        topcorner, bottomcorner = bottomcorner, topcorner
    edge = np.concatenate([topedge, topcorner])
    support = np.concatenate([bottomedge, bottomcorner])
    pf.draw(M.coords[field], color='black', marksize=8)
    pf.draw(M.coords[edge], color='red', marksize=8)
    pf.draw(M.coords[support], color='green', marksize=8)

    # Define and assign properties
    q = -0.005  # distributed load [N/mm^2]
    Q = 0.5*q*dx*dx  # equivalent concentrated load in the nodes
    P = fe.PropertyDB()
    P.nodeProp(set=field, cload=[0, 0, Q, 0, 0, 0])
    P.nodeProp(set=edge, cload=[0, 0, Q/2, 0, 0, 0])
    P.nodeProp(set=support, bound=[1, 1, 1, 0, 0, 0])

    circ20 = fe.ElemSection(
        section={
            'name': 'circ20',
            'sectiontype': 'Circ',
            'radius': 10,
            'cross_section': 314.159
        },
        material={
            'name': 'S500',
            'young_modulus': 210000,
            'shear_modulus': 81000,
            'poisson_ratio': 0.3,
            'yield_stress': 500,
            'density': 0.000007850
        }
    )

    # example of how to set the element type by set
    P.elemProp(set=topbar, section=circ20, eltype='T3D2')
    P.elemProp(set=bottombar, section=circ20, eltype='T3D2')
    P.elemProp(set=diabar, section=circ20, eltype='T3D2'),
    # Since all elements have same characteristics, we could just have used:
    #   P.elemProp(section=circ20,elemtype='T3D2')
    # But putting the elems in three sets allows for separate postprocessing
    return M, P


def run():
    """Create FEA model and input file for an Abaqus simulation"""
    pf.frontview('xz')
    # !! The negative height is to have the largest dech at the bottom
    M, P = createModel(1800., -900., 4, 5)

    # Print node and element property databases
    P.print()

    # Write the Abaqus inputfile
    step = fe.Step(
        out=[fe.Output(type='field')],
        res=[fe.Result(kind='element', keys=['S']),
             fe.Result(kind='node', keys=['U'])
             ]
    )
    model = fe.FEModel(M)
    if not pf.checkWorkdir():
        return
    inpfile = fe.AbqData(model, P, [step], eprop=M.prop).write('SpaceTruss')
    if pf.ack("Show the .INP file?"):
        pf.showFile(inpfile)


if __name__ == '__draw__':
    run()

# End
