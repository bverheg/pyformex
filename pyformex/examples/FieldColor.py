#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""FieldColor

This examples shows how to interactively change the colors displayed
on an object.

.. metadata
  :level: normal
  :topics: image
  :techniques: color, field, field color, slider
"""
import pyformex as pf
from pyformex import _I
_name = pf.Path(__file__).stem


def changeColorDirect(i):
    """Change the displayed color by directly changing color attribute."""
    global FA, model
    F = FA.object
    color = F.getField('color_%s' % i)
    if model == 'Quads':
        color = color.convert('elemn')
    color = color.data
    print("Color shape = %s" % str(color.shape))
    # print("Drawables: %s:" % len(FA.drawable))
    for dr in FA.drawable[:2]:
        dr.changeVertexColor(color)
    pf.canvas.update()


def setFrame(item):
    changeColorDirect(item.value())


def run():
    global width, height, FA, dialog, dotsize, model
    pf.clear()
    pf.smooth()
    pf.lights(False)
    pf.view('front')

    # read an image from pyFormex data
    filename = pf.cfg['datadir'] / 'butterfly.png'
    print(filename)
    color = pf.image2array(filename, gl=True)
    print(color.shape, color.dtype, color.min(), color.max())
    ny, nx = color.shape[:2]
    print("Image size: %s x %s" % (nx, ny))

    # Create a 2D grid of nx*ny elements
    res = pf.askItems([_I('model', itemtype='hradio', choices=['Dots', 'Quads'])])
    if not res:
        return

    model = res['model']
    dotsize = 3
    if model == 'Dots':
        base = '1:0'
    else:
        base = '4:0123'
    F = pf.Formex(base).replicm((nx, ny)).toMesh()

    # Currently, Field Color changes will only work with VertexColor.
    # Therefore, when using Quads, multiplex the color to vertexcolor.
    # if model == 'Quads':
    #       color = multiplex(color,4,axis=-2)
    color = color.reshape(F.nelems(), 3)  # makes sure we got it right
    print("Color shape = %s" % str(color.shape))
    FA = pf.draw(F, color=color, marksize=dotsize, name='image')
    # zoomAll()

    # Create some color fields
    # The color fields consist of the original image with one of
    # the standard colors mixed in.
    nframes = 10
    mix = [0.7, 0.3]
    for i in range(nframes):
        fld = mix[0] * color + mix[1] * pf.canvas.settings.colormap[i]
        # fldtype = 'node' if model == 'Dots' else 'elemc'
        fldtype = 'elemc'
        F.addField(fldtype, fld, 'color_%s' % i)

    frame = 0
    dialog = pf.Dialog([
        _I('frame', frame, itemtype='slider', min=0, max=nframes-1, ticks=1,
           func=setFrame, tooltip="Move the slider to see one of the color"
           "fields displayed on the original Actor"),
    ])
    dialog.setMinimumWidth(500)
    dialog.show()


if __name__ == '__draw__':
    run()

# End
