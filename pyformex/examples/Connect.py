#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Connect

Illustrates the possibilities of the pf.connect function

.. metadata
  :level: normal
  :topics: formex, surface
  :techniques: connect, color
"""
import pyformex as pf
_name = pf.Path(__file__).stem


def main():
    pf.clear()
    pf.linewidth(2)
    pf.flatwire()
    pf.setDrawOptions({'bbox': 'auto'})

    # A tapered grid of points
    F = pf.Formex([0.]).replic2(10, 5, taper=-1)
    pf.draw(F)

    # Split in parts by testing y-position; note use of implicit loop!
    G = [F.clip(F.test(dir=1, min=i-0.5, max=i+0.5)) for i in range(5)]
    print([Gi.nelems() for Gi in G])

    def annot(char):
        [pf.drawText(f"{char}{i}", G[i][0, 0]+[-0.5, 0., 0.])
         for i, Gi in enumerate(G)]

    # Apply a general mapping function : x,y,x -> [ newx, newy, newz ]
    G = [Gi.map(lambda x, y, z:[x, y+0.01*float(i+1)**1.5*x**2, z])
         for i, Gi in enumerate(G)]
    pf.clear()
    pf.draw(G)
    annot('G')
    pf.setDrawOptions({'bbox': 'last'})
    # Connect G0 with G1
    H1 = pf.connect([G[0], G[1]])
    pf.draw(H1, color='blue')
    # Connect G1 with G2 with a 2-element bias
    H2 = pf.connect([G[1], G[2]], bias=[0, 2])
    pf.draw(H2, color='green')
    # Connect G3 with G4 with a 1-element bias plus loop
    H2 = pf.connect([G[3], G[4]], bias=[1, 0], loop=True)
    pf.draw(H2, color='red')
    # Create a triangular grid of bars
    pf.clear()
    pf.draw(G)
    annot('G')
    # Connect Gi[j] with Gi[j+1] to create horizontals
    K1 = [pf.connect([i, i], bias=[0, 1]) for i in G]
    pf.draw(K1, color='blue')
    # Connect Gi[j] with Gi+1[j] to create verticals
    K2 = [pf.connect([i, j]) for i, j in zip(G[:-1], G[1:])]
    pf.draw(K2, color='red')
    # Connect Gi[j+1] with Gi+1[j] to create diagonals
    K3 = [pf.connect([i, j], bias=[1, 0]) for i, j in zip(G[:-1], G[1:])]
    pf.draw(K3, color='green')
    # Create triangles
    pf.clear()
    pf.draw(G)
    annot('G')
    L1 = [pf.connect([i, i, j], bias=[0, 1, 0]) for i, j in zip(G[:-1], G[1:])]
    pf.draw(L1, color='red')
    L2 = [pf.connect([i, j, j], bias=[1, 0, 1]) for i, j in zip(G[:-1], G[1:])]
    pf.draw(L2, color='green')
    # Connecting multiplex Formices using bias
    pf.clear()
    annot('K')
    pf.draw(K1)
    L1 = [pf.connect([i, i, j], bias=[0, 1, 0]) for i, j in zip(K1[:-1], K1[1:])]
    pf.draw(L1, color='red')
    L2 = [pf.connect([i, j, j], bias=[1, 0, 1]) for i, j in zip(K1[:-1], K1[1:])]
    pf.draw(L2, color='green')
    # Connecting multiplex Formices using nodid
    pf.clear()
    pf.draw(K1)
    annot('K')
    L1 = [pf.connect([i, i, j], nodid=[0, 1, 0]) for i, j in zip(K1[:-1], K1[1:])]
    pf.draw(L1, color='red')
    L2 = [pf.connect([i, j, j], nodid=[1, 0, 1]) for i, j in zip(K1[:-1], K1[1:])]
    pf.draw(L2, color='green')
    # Add the missing end triangles
    L3 = [pf.connect([i, i, j], nodid=[0, 1, 1],
                     bias=[i.nelems()-1, i.nelems()-1, j.nelems()-1])
          for i, j in zip(K1[:-1], K1[1:])]
    pf.draw(L3, color='magenta')
    # Collect all triangles in a single Formex
    L = ((pf.Formex.concatenate(L1) + pf.Formex.concatenate(L3)).setProp(1) +
         pf.Formex.concatenate(L2).setProp(2))
    pf.clear()
    pf.draw(L)
    # Convert to a Mesh
    print(f"{L.nelems()=}, {L.nplex()=}, {L.coords.shape=}")
    M = L.toMesh()
    print(f"{M.nelems()=}, {M.nplex()=}, {M.coords.shape=}")
    pf.clear()
    pf.draw(M, color='yellow', mode='flatwire')
    pf.drawNumbers(M)
    pf.draw(M.getBorderMesh(), color='black', linewidth=6)
    # Convert to a surface
    S = pf.TriSurface(M)
    print(f"{S.nelems()=}, {S.nplex()=}, {S.coords.shape=}")
    pf.clear()
    pf.draw(S)
    print(f"Total surface area: {S.area()}")
    pf.PF.update({'surface-1': S})
    pf.setDrawOptions({'bbox': 'auto'})


def run():
    save_delay = pf.delay(2)
    main()
    pf.delay(save_delay)


if __name__ == '__draw__':
    run()

# End
