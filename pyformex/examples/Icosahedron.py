#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Icosahedron

Draws an icosahedron and its projection on a sphere.

First, an Icosahedron is constructed. Its facets are subdivided
into a number of smaller triangles and are then projected onto
a sphere inscribed inside the Icosahedron.

The result is shown constant in red color. The orginal Icosahedron
is repeatedly shown while it is shrinking, until it completely
disappears inside the red sphere. Finally, an intermediate configuration
is shown.

.. metadata
  :level: normal
  :topics: Mesh, Geometry, Sphere
  :techniques: subdivide, projection, animation
"""
import numpy as np
import pyformex as pf
_name = pf.Path(__file__).stem

def run():
    pf.smooth()
    pf.clear()

    from pyformex.arraytools import golden_ratio as phi
    s = np.sqrt(1.+phi*phi)
    a = np.sqrt(3.)/6.*(3.+np.sqrt(5.))

    I = pf.Mesh(eltype='icosa').getBorderMesh()
    M = I.subdivide(10)
    S = M.projectOnSphere()

    pf.delay(0)
    pf.draw(S, color='red')

    a0 = 1./a
    a1 = 1./s - a0
    A = pf.draw(I.scale(a0), color='yellow')
    pf.zoomAll()
    n = 100
    pf.delay(0.05)
    with pf.busyCursor():
        for i in np.arange(n+1)/float(n):
            B = pf.draw(I.scale(a0+i*a1), color='yellow', bbox='last')
            pf.undraw(A)
            A = B

    pf.delay(2)
    pf.wait()
    pf.draw(I.scale(1./phi), color='yellow')
    pf.undraw(A)
    pf.delay(0)


if __name__ == '__draw__':
    run()

# End
