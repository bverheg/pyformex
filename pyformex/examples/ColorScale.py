#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""ColorScale

Example showing the use of the 'colorscale' plugin.

.. metadata
  :level: normal
  :topics: FEA
  :techniques: dialog, color
"""
import pyformex as pf
from pyformex import _I, _G, _C, _T
_name = pf.Path(__file__).stem

dialog = None
def show():
    """Accept the data and draw according to them"""
    global medval, medcol, palet, paltype, minexp, grid, nlabels, dialog

    if not dialog.validate():
        return
    pf.clear()
    pf.lights(False)
    pf.PF['_ColorScale_data_'] = dialog.results

    res = dialog.results
    if res.pop('valrange') == 'Minimum-Maximum':
        res['medval'] = None
        res['minexp'] = None

    res.pop('header')
    res.pop('gravity')
    paltype = res.pop('paltype')
    palet = tuple(pf.Color(res.pop(i)) for i in ('mincol', 'medcol', 'maxcol'))
    if res.pop('custom'):
        if paltype == '2-color':
            res['palet'] = (palet[0], None, palet[2])
        else:
            res['palet'] = palet

    x, y = res.pop('position')
    w, h = res.pop('size')
    if res.pop('autosize'):
        mw, mh = pf.canvas.getSize()
        h = int(0.9*(mh-y))
        w = min(0.1*mw, 100)
    if res.pop('autopos'):
        x = 100
    res['x'] = x
    res['y'] = y
    res['w'] = w
    res['h'] = h

    # ok, now draw it
    drawColorScale(**res)


def drawColorScale(palet, minval, maxval, medval, maxexp, minexp,
                   ncolors, dec, scale, ngrid, linewidth, nlabel,
                   lefttext, textsize, textcolor, x, y, w, h):
    """Draw a color scale with the specified parameters"""
    CS = pf.ColorScale(palet, minval, maxval, midval=medval,
                       exp=maxexp, exp2=minexp)
    CLA = pf.ColorLegend(CS, ncolors, x, y, w, h,
                         ngrid=ngrid, linewidth=linewidth,
                         nlabel=nlabel, size=textsize, dec=dec, scale=scale,
                         lefttext=lefttext, textcolor=textcolor)
    pf.drawActor(CLA)


def close():
    global dialog
    ## pf.PF['ColorScale_data'] = dialog.results
    if dialog:
        dialog.close()
        dialog = None
    # Release script.ock
    pf.script.scriptRelease(__file__)


def timeOut():
    """What to do on a Dialog timeout event.

    As a policy, all pyFormex examples should behave well on a
    dialog timeout.
    Most users can simply ignore this.
    """
    show()
    close()


def atUnload():
    print("Unloading? Wait, I want to close the dialog first")
    close()


dialog = None
def run():
    global dialog
    if dialog:
        return  # avoid multiple executions

    pf.clear()
    # Create the modeless dialog widget
    dialog = pf.Dialog(caption=_name, store=_name+'_data', items=[
        _C('', [
            _I('valrange', text='Value range type',
               choices=['Minimum-Medium-Maximum', 'Minimum-Maximum']),
            _I('maxval', 12.0, text='Maximum value'),
            _I('medval', 0.0, text='Medium value'),
            _I('minval', -6.0, text='Minimum value'),
            _I('palet', text='Predefined color palette', value='RAINBOW',
                choices=list(pf.Palette.keys())),
            _G('custom', text='Custom color palette', items=[
                _I('paltype', text='Palette type', choices=['3-color', '2-color']),
                _I('maxcol', [1., 0., 0.], text='Maximum color', itemtype='color'),
                _I('medcol', [1., 1., 0.], text='Medium color', itemtype='color'),
                _I('mincol', [1., 1., 1.], text='Minimum color', itemtype='color'),
            ], check=False),
            _I('maxexp', 1.0, text='High exponent'),
            _I('minexp', 1.0, text='Low exponent'),
            _I('ncolors', 200, text='Number of colors'),
        ]),
        _C('', [
            _T('Grid/Label', [
                _I('ngrid', -1, text='Number of grid intervals'),
                _I('linewidth', 1.5, text='Line width'),
                _I('nlabel', -1, text='Number of label intervals'),
                _I('dec', 2, text='Decimals'),
                _I('scale', 0, text='Scaling exponent'),
                _I('lefttext', True, text='Text left of colorscale'),
                _I('textsize', 18, text='Text height'),
                _I('textcolor', pf.canvas.settings.fgcolor, text='Text color',
                    itemtype='color'),
                _I('header', 'Currently not displayed', text='Header',
                    enabled=False),
                _I('gravity', 'Notused', text='Gravity', enabled=False),
            ]),
            _T('Position/Size', [
                _I('autosize', True, text='Autosize'),
                _I('size', (100, 600), text='Size'),
                _I('autopos', True, text='Autoposition'),
                _I('position', [100, 50], text='Position'),
            ]),
        ]),
    ], enablers=[
        ('valrange', 'Minimum-Medium-Maximum', 'medval'),
        ('paltype', '3-color', 'medcol'),
        ('custom', False, 'palet'),
        ('autosize', False, 'size'),
        ('autopos', False, 'position'),
    ], actions=[('Close', close), ('Show', show)], default='Show')

    # Show the dialog and let the user have fun
    dialog.show(timeoutfunc=timeOut)
    # Block other scripts
    pf.script.scriptLock(__file__)


if __name__ == '__draw__':
    run()

# End
