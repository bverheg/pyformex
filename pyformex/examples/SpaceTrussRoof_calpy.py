#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Double Layer Flat Space Truss Roof

.. metadata
  :level: advanced
  :topics: FEA
  :techniques: dialog, animation, persistence, color
"""
import numpy as np
import pyformex as pf
from pyformex import _I
# from pyformex import fe
# from pyformex.examples.SpaceTrussRoof import createRoof
from pyformex.examples.SpaceTrussRoof_abq import createModel
_name = pf.Path(__file__).stem


def run():
    """The main function: execute the subsequent steps"""
    global fe_util, truss3d
    pf.frontview('xz')

    # Check that we have calpy
    if not check_calpy():
        return

    # Preprocessing
    model = run_preproc()

    # Simulation
    results = run_calpy(*model)

    # Postprocessing
    if pf.options.gui:
        run_postproc(model[0], *results)


def check_calpy():
    """Check that we have calpy modules fe_util and truss3d"""
    global fe_util, truss3d
    try:
        ############################
        # Load the needed calpy modules
        from pyformex.plugins import calpy_itf  # noqa: 401
        print("Loading calpy")
        import calpy
        calpy.options.optimize = False
        print("Loading calpy.fe_util")
        from calpy import fe_util
        print("Loading calpy.truss3d")
        from calpy import truss3d
        return True
        ############################
    except Exception:
        pf.warning("Calpy could not be loaded")
        return False


def run_preproc():
    dx = 1800.
    M, P = createModel(dx, -1500., 8, 6)

    # Create calpy model
    #####################

    # Materials
    matnr = np.zeros_like(M.prop)
    mats = []
    m = 0
    for p in P.getProp(kind='e', attr=['section']):
        mats.append((p.young_modulus, p.density, p.cross_section))
        matnr[p.set] = m = m+1
    mats = np.array(mats)
    # Calpy wants matnrs and elems in single array
    matnod = np.concatenate([matnr.reshape((-1, 1)), M.elems+1], axis=-1)

    # Boundary conditions
    nnod = M.ncoords()
    dofs = [0, 1, 2]   # the active DOFs
    bcon = np.zeros([nnod, len(dofs)], dtype=int)
    for p in P.getProp(kind='n', attr=['bound']):
        bcon[p.set] = p.bound[dofs]

    # number the equations
    fe_util.NumberEquations(bcon)
    ndof = bcon.max()
    print(f"{ndof=}")

    # Loads
    nlc = 1  # number of load cases
    loads = np.zeros((ndof, nlc), pf.Float)
    # assemble concentrated loads
    for p in P.getProp(kind='n', attr=['cload']):
        cload = np.zeros(len(dofs), pf.Float)
        for dof, load in p.cload:
            cload[dof] = load
        for n in p.set:
            loads[:, 0] = fe_util.AssembleVector(
                loads[:, 0], cload, bcon[n, :])
    return M, bcon, mats, matnod, loads


def run_calpy(M, bcon, mats, matnod, loads):
    """Run the CalPy analysis"""
    import time
    from contextlib import redirect_stdout
    print("Performing analysis: this may take some time")
    if pf.checkWorkdir():
        indir = pf.cfg['workdir']
    else:
        tmpdir = pf.TempDir()
        indir = tmpdir.path
    outfile = indir / pf.Path(__file__).with_suffix('.out')
    print("Output is written to file '%s'" % outfile.resolve())
    with outfile.open('w') as fil:
        with redirect_stdout(fil):
            print("# File created by pyFormex on %s" % time.ctime())
            print("# Script name: %s" % pf.script.scriptname)
            displ, frc = truss3d.static(M.coords, bcon, mats, matnod, loads,
                                        Echo=True)
            print("# Analysis finished on %s" % time.ctime())
    return outfile, displ, frc


def run_postproc(M, outfile, displ, frc):
    """Run the postprocessing"""
    from pyformex.fe.postproc import frameScale
    FA = TA = None

    def showOutput():
        pf.showFile(outfile)

    def showForces():
        # Give the mesh some meaningful colors.
        # The frc array returns element forces and has shape
        #  (nelems,nforcevalues,nloadcases)
        # In this case there is only one resultant force per element (the
        # normal force), and only load case; we still need to select the
        # scalar element result values from the array into a onedimensional
        # vector val.
        val = frc[:, 0, 0]
        # create a colorscale
        CS = pf.ColorScale(['blue', 'yellow', 'red'], val.min(), val.max(),
                           0., 2., 2.)
        cval = np.array([CS.color(v) for v in val])
        pf.clear()
        pf.linewidth(3)
        pf.draw(M, color=cval)
        pf.drawText('Normal force in the truss members', (300, 50), size=14)
        CLA = pf.ColorLegend(CS, 100, 10, 10, 30, 200, size=14)
        pf.decorate(CLA)

    def deformed_plot(dscale=100.):
        """Shows a deformed plot with deformations scaled with a factor scale."""
        nonlocal FA, TA
        dnodes = M.coords + dscale * displ[:, :, 0]
        deformed = pf.Mesh(dnodes, M.elems, M.prop)
        FA = pf.draw(deformed, bbox='last', view=None, wait=False, undraw=FA)
        TA = pf.drawText('Deformed geometry (scale {dscale:2f})',
                         (300, 50), size=24)
        return TA

    def animate_deformed_plot(amplitude, sleeptime=1, count=1):
        """Shows an animation of the deformation plot using nframes."""
        nonlocal FA, TA
        pf.clear()
        while count > 0:
            count -= 1
            for s in amplitude:
                T = deformed_plot(s)
                pf.undraw(TA)
                TA = T
                pf.sleep(sleeptime)

    def getOptimscale():
        """Determine an optimal scale for displaying the deformation"""
        siz0 = M.sizes()
        dM = pf.Mesh(displ[:, :, 0], M.elems)
        siz1 = dM.sizes()
        return pf.at.niceNumber(1./(siz1/siz0).max())


    def showDeformation():
        pf.clear()
        pf.linewidth(1)
        pf.draw(M, color='black')
        pf.linewidth(3)
        deformed_plot(optimscale)
        pf.view('last', True)


    def showAnimatedDeformation():
        """Show animated deformation"""
        nframes = 10
        res = pf.askItems([
            _I('scale', optimscale),
            _I('nframes', nframes),
            _I('form', 'revert', choices=['up', 'updown', 'revert']),
            _I('duration', 5./nframes),
            _I('ncycles', 2),
        ], caption='Animation Parameters')
        if res:
            scale = res['scale']
            nframes = res['nframes']
            form = res['form']
            duration = res['duration']
            ncycles = res['ncycles']
            amp = scale * frameScale(nframes, form)
            animate_deformed_plot(amp, duration, ncycles)


    optimscale = getOptimscale()
    options = {
        'None': None,
        'Output File': showOutput,
        'Member forces': showForces,
        'Deformation': showDeformation,
        'Animated deformation': showAnimatedDeformation,
    }
    cnt = 0
    while True and cnt < 1000:
        cnt += 1
        ans = pf.ask("Which results do you want to see?", list(options.keys()))
        func = options[ans]
        if func is None:
            break
        func()
        if pf.gui.widgets.input_timeout > 0:  # timeout
            break


if __name__ == '__draw__':
    run()

# End
