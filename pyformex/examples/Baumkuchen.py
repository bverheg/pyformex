#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Baumkuchen Vault

This example creates and displays a Baumkuchen vault.
The Baumkuchen structure in created in a parametric function, so it
can be imported and used in other applications.

.. metadata
  :level: beginner
  :topics: structure
  :techniques: color, bump
"""
import numpy as np
import pyformex as pf
_name = pf.Path(__file__).stem


def Baumkuchen(m, n, k, e1, e2, beam=False, return_intermediate=False):
    """Create a Baumkuchen vault

    Parameters
    ----------
    m: int
        The number of cells in x-direction
    n: int
        The number of cells in y-direction
    k: int
        The number of vaults in x-direction
    e1: float
        The elevation of the major arcs
    e2: float
        The elevation of the minor arcs
    beam: bool
        If False (default), the vault consists of quadrilateral shell
        elements. If True, a beam structure is created.
    return_intermediate: bool
        If False (default), only returns the final structure.
        If True, also returns the intermediate steps in the construction
        of the Baumkuchen vault. This is especially useful for educational
        purposes.

    Returns
    -------
    Formex
        A Formex with quadrilateral elements forming a Baumkuchen vault.
        If return_intermediate is True, returns a tuple of four Formex
        structures: the m x n grid in the x-y-plane, the grid bumped in
        x-direction, the previous with an extra bump in y-direction,
        and the final structure.
    """
    # Create a grid of quads, m in direction x, n in direction y
    if beam:
        # Create a grid of beam elements
        a1 = pf.Formex('l:2').replicm((m+1, n)) + \
            pf.Formex('l:1').replicm((m, n+1))
    else:
        # Create a grid of quad elements
        a1 = pf.Formex('4:0123').replicm((m, n))
    # Add a z-bump varying in x-direction
    p = np.array(a1.center())
    p[2] = e1
    a2 = a1.bump(2, p, lambda x: 1-(x/18)**2/2, 1)
    # Add another z-bump varying in y-direction
    p[2] = e2
    a3 = a2.bump(2, p, lambda x: 1-(x/6)**2/2, 0)
    # Replicate the structure in x-direction
    a4 = a3.replicate(k, dir=0, step=m)
    if return_intermediate:
        return a1, a2, a3, a4
    else:
        return a4

def run():
    global a1, a2, a3, a4
    pf.clear()
    pf.smoothwire()
    a1, a2, a3, a4 = Baumkuchen(12, 36, 7, 30, 5, False, True)
    pf.delay(1)
    pf.draw(a1, view='xy')
    pf.draw(a2, view='xz', color='red')
    pf.draw(a3, view='xz', color='green')
    pf.draw(a4, view='xz', color='blue')
    pf.delay(0)


if __name__ == '__draw__':
    run()
# End
