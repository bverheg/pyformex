#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Supershape

This example illustrates the power of the superspherical transformation.
It is also a nice example of dedicated GUI construction and of the use
of files to store data.

The example creates sphere-like structures by applying a superspherical
transformation to a planar grid. The parameters defining the grid and the
transformation can be set from a modeless GUI. The grid as well as the
resulting structure can be shown. The parameters can also be saved to a
file. Finally, the GUi also allows to replay the whole set of saved
structures.

.. metadata
  :level: advanced
  :topic: geometry
  :techniques: dialog, persistence
"""
import pyformex as pf
from pyformex import _I, _G
_name = pf.Path(__file__).stem
_data = _name+'_data'

dialog = None
savefile = None
tol = 1.e-4

gname = pf.NameSequence('Grid-0')
sname = pf.NameSequence('Shape-0')

grid_data = [
    _I('grid_size', [24, 12],),
    _I('x_range', (-180., 180.),),
    _I('y_range', (-90., 90.),),
    _I('grid_base', 'quad', itemtype='radio',
       choices=['quad', 'tri-u', 'tri-d', 'tri-x']),
    _I('grid_bias', 0.0,),
    _I('grid_skewness', 0.0,),
    _I('x_clip', (-360., 360.),),
    _I('y_clip', (-90., 90.),),
    _I('grid_name', gname.peek(),),
    _I('grid_color', 'blue',),
]

def createGrid(grid_size, x_range, y_range, grid_base, grid_bias,
               grid_skewness, x_clip, y_clip, grid_name, grid_color, **kargs):
    """Create the grid from global parameters"""
    global B
    nx, ny = grid_size
    b, h = x_range[1]-x_range[0], y_range[1]-y_range[0]
    if grid_base.startswith('tri'):
        diag = grid_base[-1]
    else:
        diag = ''
    B = pf.simple.rectangle(nx, ny, b, h, diag=diag, bias=grid_bias).translate(
        [x_range[0], y_range[0], 1.])
    if grid_skewness != 0.0:
        B = B.shear(0, 1, grid_skewness*b*ny/(h*nx))
    if x_clip:
        B = B.clip(B.test('any', dir=0, min=x_clip[0]+tol*b, max=x_clip[1]-tol*b))
    if y_clip:
        B = B.clip(B.test('any', dir=1, min=y_clip[0]+tol*h, max=y_clip[1]-tol*h))
    B.attrib(color=grid_color)
    pf.PF[grid_name] = B


shape_data = [
    _I('north_south', 1.0,),
    _I('east_west', 1.0,),
    _I('eggness', 0.0,),
    _I('scale', [1., 1., 1.],),
    _I('post', '',),
    _I('name', sname.peek(),),
    _I('color', 'red',),
    _I('bkcolor', 'yellow',),
    # _I('texture','None',),
]


def createSuperShape(north_south, east_west, eggness, scale, post, name,
                     color, bkcolor, grid_name, **kargs):
    """Create a super shape from global parameters"""
    global F
    F = B.superSpherical(n=north_south, e=east_west, k=eggness)
    if scale == [1.0, 1.0, 1.0]:
        pass
    else:
        F = F.scale(scale)
    if post:
        print("Post transformation")
        F = eval(post)
    pf.PF[name] = F


def drawGrid():
    """Show the last created grid"""
    print("DRAWING GRID")
    pf.clear()
    pf.wireframe()
    pf.view('front')
    pf.draw(B)


def drawSuperShape(color, bkcolor, grid_size, **kargs):
    """Show the last created super shape"""
    pf.clear()
    pf.smoothwire()
    if isinstance(color, str) and color.startswith('file:'):
        print("trying to convert color")
        filename = 'Elevation-800.jpg'
        nx, ny = grid_size
        color = pf.image2array(filename, size=(nx, ny), gl=True)
        print(f"{color.shape=}")
    pf.draw(F, color=color, bkcolor=bkcolor)


def acceptData():
    if dialog.validate():
        pf.PF[_data] = dialog.results
        return True
    return False

##########################
# Button Functions

def showGrid():
    """Accept data, create and show grid."""
    if acceptData():
        data = pf.PF[_data]
        print(f"{data=}")
        createGrid(**data)
        print("CREATED GRID, will now draw it")
        drawGrid()

def replayShape():
    """Create and show grid from current data."""
    data = pf.PF[_data]
    createGrid(**data)
    createSuperShape(**data)
    drawSuperShape(**data)

def show():
    """Accept data, create and show shape."""
    if acceptData():
        replayShape()


def close():
    global dialog, savefile
    if dialog:
        dialog.close()
        dialog = None
    if savefile:
        savefile.close()
        savefile = None
    pf.script.scriptRelease(__file__)


def save():
    global savefile
    show()
    if savefile is None:
        filename = pf.askFilename(filter="Text files (*.txt)")
        if filename:
            savefile = open(filename, 'a')
    if savefile:
        print("Saving to file")
        savefile.write(f"{dialog.results}\n")
        savefile.flush()
        grid_name = next(gname)
        name = next(sname)
        pf.PF[_data].update({'grid_name': grid_name, 'name': sname, })
        if dialog:
            dialog['grid_name'].setValue(grid_name)
            dialog['name'].setValue(name)


def replay():
    global savefile
    if savefile:
        filename = savefile.name
        savefile.close()
    else:
        filename = pf.cfg['datadir'] / 'supershape.txt'
        filename = pf.askFilename(filename, filter="Text files (*.txt)")
    if filename:
        savefile = open(filename, 'r')
        for line in savefile:
            print(line)
            globals().update(eval(line))
            replayShape()
        savefile = open(filename, 'a')


################# Dialog

dialog_actions = [
    ('Close', close),
    ('Reset', pf.reset),
    ('Replay', replay),
    ('Save', save),
    ('Grid', showGrid),
    ('Show', show)
]

dialog_default = 'Show'


def timeOut():
    try:
        show()
    finally:
        close()


def createDialog():
    global dialog

    # Create the dialog
    dialog = pf.Dialog(caption=_name, store=_name+'_data', items=[
        _G('Grid data', grid_data),
        _G('Shape data', shape_data),
    ], actions=dialog_actions, default=dialog_default)


def run():
    """Show the dialog"""
    pf.resetAll()
    pf.clear()
    pf.smoothwire()
    pf.lights(True)
    pf.transparent(False)
    createDialog()
    pf.setView('eggview', (0., -30., 0.))
    pf.view('eggview')
    dialog.show(timeoutfunc=timeOut)
    pf.script.scriptLock(__file__)


if __name__ == '__draw__':
    run()

# End
