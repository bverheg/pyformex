#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Voxelize

This example illustrates the use of the gtsinside program to create a
voxelization of a closed surface.

.. metadata
  :level: advanced
  :topics: surface
  :techniques: voxelize, image
"""
import numpy as np
import pyformex as pf
from pyformex import _I, _G
from pyformex.plugins.imagearray import saveGreyImage
_name = pf.Path(__file__).stem

filename = pf.cfg['datadir'] / 'horse.off'

def createSurface(surface, filename, grade, **kargs):
    """Create and draw a closed surface from input data

    """
    if surface == 'file':
        S = pf.TriSurface.read(filename).centered()
    elif surface == 'sphere':
        S = pf.simple.sphere(ndiv=grade)

    pf.draw(S)

    if not S.isClosedManifold():
        pf.warning("This is not a closed manifold surface. Try another.")
        return None
    return S


# TODO: this should be merged with opengl.drawImage3D
def showGreyImage(a):
    """Draw pixel array on the canvas"""
    F = pf.Formex('4:0123').repm([a.shape[1], a.shape[0]], [0, 1], [1., 1.]).setProp(a)
    return pf.draw(F)


def saveScan(scandata, surface, filename, showimages=False, **kargs):
    """Save the scandata for the surface"""
    dirname = pf.askDirname(caption="Directory where to store the images")
    if not dirname:
        return
    pf.chdir(dirname)
    if not pf.checkWorkdir():
        print("Could not open directory for writing. I have to stop here")
        return

    if surface == 'sphere':
        name = 'sphere'
    else:
        name = pf.utils.projectName(filename)
        fs = pf.NameSequence(name, '.png')

    if showimages:
        pf.clear()
        pf.flat()
        A = None
    for frame in scandata:
        if showimages:
            B = showGreyImage(frame*7)
            pf.undraw(A)
            A = B

        saveGreyImage(frame*255, next(fs))


def run():
    pf.resetAll()
    pf.clear()
    pf.smooth()
    pf.lights(True)

    res = pf.askItems(caption=_name, store=_name+'_data', items=[
        _G('Model', [
            _I('surface', choices=['file', 'sphere']),
            _I('filename', filename, text='Image file', itemtype='filename',
               filter='surface', mode='exist'),
            _I('grade', 8),
        ]),
        _G('Scan', [
            _I('resolution', 100),
        ]),
    ], enablers=[
        ('surface', 'file', 'filename', ),
        ('surface', 'sphere', 'grade', ),
    ])
    if not res:
        return

    S = createSurface(**res)
    if S:
        nmax = res['resolution']
        with pf.busyCursor():
            vox, P = S.voxelize(nmax, return_formex=True)
        scale = P.sizes() / np.array(vox.shape)
        origin = P.bbox()[0]
        print(f"Box size: {P.sizes()}")
        print(f"Data size: {np.array(vox.shape)}")
        print(f"Scale: {scale}")
        print(f"Origin: {origin}")
        pf.draw(P, marksize=5)
        pf.transparent()
        saveScan(vox, showimages=True, **res)


if __name__ == '__draw__':
    run()

# End
