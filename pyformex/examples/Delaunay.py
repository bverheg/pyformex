#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Delaunay

This example demonstrates Delaunay triangulation using the gtsdelaunay
function from pyformex.plugins.gts_itf.

.. metadata
  :level: normal
  :topics: surface
  :techniques: delaunay, random, pattern
"""
import random
import numpy as np
import pyformex as pf
from pyformex import _I
from pyformex.plugins.gts_itf import gtsdelaunay
_name = pf.Path(__file__).stem
pf.Module.require('pyformex', '>=4.0')

pf.chdir(__file__)
pf.mkdir('temp')
pf.smoothwire()

atoms2D = '0123456789/'
atoms3D = atoms2D + 'ABCDEFGHI' + 'abcdefghi'
atoms = {2: atoms2D, 3: atoms3D}

def randompat(atoms, length):
    return ''.join(random.choices(atoms, k=length))

pf.delay(0)

def randomPL(length=20):
    pat = randompat(atoms, length)
    pf.clear()
    X = pf.Coords(pat)
    PL = pf.PolyLine(X)
    pf.draw(X, marksize=8)
    #pf.drawNumbers(X, gravity='ne')
    pf.draw(PL, color='red')
    return pat, X, PL

def gensurface(dimension, method, npoints):
    ndim = int(dimension[0])
    meth = method.split()[1][1]
    if meth == 'a':
        X = pf.Coords(randompat(atoms[ndim], npoints))
    else:
         X = pf.Coords(np.random.random((npoints, ndim)))
    if ndim == 3:
        X.z *= 0.1
    pf.clear()
    pf.draw(X, marksize=8)
    X = X.fuse()[0]
    print(f"Number of points: {npoints} created, {X.shape[0]} unique")
    S = gtsdelaunay(X, options='-d', keep='temp')
    print(S)
    pf.draw(S, color='yellow')
    pf.PF['S'] = S


class dialog:
    _dialog = None

    @staticmethod
    def create():
        """Create the dialog"""
        dialog._dialog = pf.Dialog(caption=_name, store=_name+'_data', items=[
            _I("dimension", choices=["2D", "3D"],
               itemtype='hradio', text="Object dimensionality"),
            _I("method", choices=["Random points", "Random pattern"],
               itemtype='hradio', text="Point generation method"),
            _I("npoints", 10, text="Number of generated points"),
        ], actions=[('Close', dialog.close), ('Show', dialog.show)])
        dialog._dialog.show()
        return dialog._dialog

    @staticmethod
    def show():
        """Accept the data and proceed according to them"""
        if dialog._dialog.validate():
            gensurface(**dialog._dialog.results)

    @staticmethod
    def close():
        """Close the dialog"""
        dialog._dialog.close()
        dialog._dialog = None

    @staticmethod
    def timeOut():
        """What to do on a Dialog timeout event"""
        dialog.show()
        dialog.close()


dia = None

def run():
    global dia
    if dia is None:
        dia = dialog.create()
    else:
        print("Dialog already exists!")


if __name__ == '__draw__':
    run()
