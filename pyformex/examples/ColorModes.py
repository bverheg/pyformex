#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""ColorModes

This example tests all combinations of rendermode and color modes,
on a triangle and a quad Mesh

.. metadata
  :level: beginner
  :topics: surface
  :techniques: color
"""
import numpy as np
import pyformex as pf
_name = pf.Path(__file__).stem


def run():
    pf.reset()
    pf.smooth()
    pf.lights(False)

    Rendermode = ['wireframe', 'flat', 'smooth']
    Lights = [False, True]
    Shapes = ['3:012', '4:0123', ]

    color0 = None  # no color: current fgcolor
    color1 = 'red'   # single color
    color2 = ['red', 'green', 'blue']  # 3 colors: will be repeated

    pf.delay(0)
    i=0
    for shape in Shapes:
        F = pf.Formex(shape).replicm((4, 2))
        color3 = np.resize(color2, F.shape[:2])  # full color
        for c in [color0, color1, color2, color3]:
            for mode in Rendermode:
                pf.clear()
                pf.renderMode(mode)
                pf.draw(F, color=c)
                pf.zoomAll()
                for light in Lights:
                    pf.lights(light)
                    print(f"{i}: color={c}, {mode=}, {light=}")
                    i += 1
                    pf.pause()


if __name__ == '__draw__':
    run()
# End
