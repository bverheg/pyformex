#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""IsoSurface

This example illustrates the use of the isosurface plugin to construct
isosurface through a set of data

.. metadata
  :level: beginner
  :topics: surface
  :techniques: isosurface, timer
"""
import numpy as np
import pyformex as pf
from pyformex import _I
from pyformex.plugins import isosurface as sf
from pyformex.plugins.dicomstack import dicom2numpy
_name = pf.Path(__file__).stem


def loadImage(file, grey=True):
    """Load a grey image into a numpy array"""
    im = pf.image2array(file)
    return im


def loadImages(files, glmode=True):
    images = [loadImage(f) for f in files]
    images = np.dstack(images)
    if images.dtype.kind == 'u':
        images = images/255.
    return images


def run():
    pf.clear()
    pf.smooth()

    options = ["Cancel", "Image files", "DICOM files", "Generated from function"]
    ans = pf.ask("This IsoSurface example can either reconstruct a surface"
                 " from a series of 2D images, or it can use data generated"
                 " from a function. Use which data?", options)
    ans = options.index(ans)
    if ans == 0:
        return

    elif ans in [1, 2]:
        fn = pf.askFilename(mode='multi')
        if not fn:
            return

        if len(fn) == 1:
            files = pf.Path(pf.NameSequence(fn[0]).glob()).glob()
        else:
            files = fn

        files = sorted(files, key=pf.sortkey('h'))
        print(files)

        # skip some files?
        res = pf.askItems([('file_step', 1)])
        if not res:
            return

        step = res['file_step']
        if step > 1:
            files = files[::step]
            print(files)

        with pf.busyCursor():
            if ans == 1:
                data = loadImages(files)
                scale = np.ones(3)
            else:
                data, scale = dicom2numpy(files)
                print(f"Spacing: {scale}")
                # normalize
                dmin, dmax = data.min(), data.max()
                data = (data-dmin).astype(np.float32)/(dmax-dmin)

        # level at which the isosurface is computed
        res = pf.askItems([
            _I('isolevel', 0.5),
            _I('algorithm', itemtype='hradio', choices=['cubes', 'tetrahedrons']),
        ])
        if not res:
            return

        level = res['isolevel']
        if level <= 0.0 or level >= 1.0:
            level = data.mean()

        tet = res['algorithm'] == 'tetrahedrons'

    else:
        # data space: create a grid to visualize
        if pf.ack("Large model (takes some time)?"):
            nx, ny, nz = 100, 150, 200  # for time testing
        else:
            nx, ny, nz = 6, 8, 10
        # F = elements.Hex8.toFormex().repm([nz, ny, nx]).setProp(1)
        # draw(F,mode='wireframe')

        # function to generate data: the distance from the origin
        # dist = lambda x, y, z: np.sqrt(x*x+y*y+z*z)
        # a bit more spectacular:
        dist = lambda x, y, z: np.sin(np.sqrt(x*y*z)/20)
        data = np.fromfunction(dist, (nz+1, ny+1, nx+1))
        scale = np.ones(3)

        # level at which the isosurface is computed
        level = 0.5*data.max()
        tet = False

    print(f"Image Data: {data.shape}, {data.dtype}")
    print(f"Pixel Spacing: {scale}")
    print(f"{data.min()=:.6f}, {data.max()=:.6f}")
    print(f"iso{level=:.6f}")

    # Compute the isosurface
    with pf.busyCursor():
        with pf.Timing() as t:
            tri = sf.isosurface(data, level, nproc=4, tet=tet)
        ntri = len(tri)
        print(f"Got {ntri} triangles in {t.mem:.6f} seconds")
        if ntri > 0:
            S = pf.TriSurface(tri).scale(scale[::-1])
            pf.draw(S, color='blue', bkcolor='red')
            pf.PF.update({'isosurf': S})


# The following is to make it work as a script
if __name__ == '__draw__':
    run()

# End
