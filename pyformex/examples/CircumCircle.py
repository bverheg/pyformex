#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""CircumCircle

.. metadata
  :level: beginner
  :topics: geometry
  :techniques: function, import, dialog, viewport
"""
import pyformex as pf
from pyformex.examples.Cube import cube_tri
_name = pf.Path(__file__).stem


def draw_circles(circles, color='red'):
    for r, c, n in circles:
        C = pf.simple.circle(r=r, n=n, c=c)
        pf.draw(C, color=color)


def drawCircles(F, func, color='red'):
    r, c, n = func(F.coords)
    pf.draw(c, color=color)
    draw_circles(zip(r, c, n), color=color)


def run():
    # multiple viewports is currently broken
    # layout(2)
    pf.wireframe()

    # draw in viewport 0
    # viewport(0)
    pf.view('front')
    pf.clear()
    rtri = pf.Formex('3:016932').scale([1.5, 1, 0])
    F = rtri + rtri.shear(0, 1, -0.5).trl(0, -4.0) + rtri.shear(0, 1, 0.75).trl(0, 3.0)
    pf.draw(F)

    drawCircles(F, pf.gt.triangleCircumCircle, color='red')
    pf.zoomAll()
    drawCircles(F, pf.gt.triangleInCircle, color='blue')
    drawCircles(F, pf.gt.triangleBoundingCircle, color='black')
    pf.zoomAll()


    # draw in viewport 1
    # viewport(1)
    pf.pause()
    pf.view('iso')
    pf.clear()
    F = cube_tri().toFormex()
    pf.draw(F)
    drawCircles(F, pf.gt.triangleInCircle)
    pf.zoomAll()

    # if not pf.ack("Keep both viewports ?"):
    #     print("Removing a viewport")
    #     # remove last viewport
    #     removeViewport()


if __name__ == '__draw__':
    run()
# End
