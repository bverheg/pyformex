#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""ColorImage

This example illustrates how to display and transform 2D images.
First, a 2D image is read from a file. You can select a file by clicking on
the filename button.
Then a grid type geometry is constructed. The size of the grid can be set,
and the image will be rescaled to that size.
The individual elements of the grid are attributed colors corresponding with
the image pixel values.
Finally, before the result is drawn, the geometry can be transformed into some
other shape or be projected on a another surface.

.. metadata
  :level: normal
  :topics: image
  :techniques: color, image
"""
import numpy as np
from PIL import Image
import pyformex as pf
from pyformex import _I
_name = pf.Path(__file__).stem


def loadImage(fn):
    global image, scaled_image, metadata
    image = Image.open(fn)
    if hasattr(image, 'text'):
        print(f"Metadata: {image.text}")
    else:
        print("No metadata")
    w, h = image.size
    print(f"{image.size=}")
    dialog = pf.currentDialog()
    maxsiz = 40000  # to avoid extraneous computation time
    if w*h > maxsiz:
        scale = np.sqrt(maxsiz/w/h)
        w = int(w*scale)
        h = int(h*scale)
        image = image.resize((w,h))
    return image


def drawTransformedImage(filename, transform, orientation):
    image = loadImage(filename)
    if image is None:
        return

    # Create the colors
    nx, ny = image.size
    print(f"Resized to {(nx, ny)}")
    color = pf.image2array(image, size=(nx, ny), gl=True).reshape(-1, 3)
    print(f"Converting image to color array of shape {color.shape}")

    # Create a 2D grid of nx*ny elements
    print("Creating grid")
    R = float(nx)/np.pi
    # L = float(ny)
    F = pf.Formex('4:0123').replicm((nx, ny)).centered()
    F = F.translate(2, R)

    # Transform grid
    if transform == 'cylindrical':
        F = F.cylindrical([2, 0, 1], [2., 90./float(nx), 1.]).rollAxes(-1)
    elif transform == 'spherical':
        F = F.spherical(scale=[1., 90./float(nx), 2.]).rollAxes(-1)
    elif transform == 'projected_on_cylinder':
        F = F.projectOnCylinder(2*R, 1)
    pf.clear()
    print("Drawing Colored grid")
    pf.draw(F, color=color)
    # pf.drawText('Created with pyFormex', (20, 40), size=24)
    pf.view(orientation)
    # pf.zoomAll()


def run():
    pf.resetAll()
    pf.flat()
    pf.lights(False)
    pf.transparent(False)
    # default image file
    filename = pf.cfg['datadir'] / 'butterfly.png'
    dia = pf.ImageLoadDialog(filename, extra=[
        _I('transform', itemtype='hradio', choices=(
            'flat', 'cylindrical', 'spherical', 'projected_on_cylinder')),
        _I('orientation', 'iso0'),
    ])
    res = dia.getResults()
    if res:
        drawTransformedImage(**res)


if __name__ == '__draw__':
    run()
# End
