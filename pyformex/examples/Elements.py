#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Elements

This example is intended for testing the drawing functions for each of the
implemented element types.

.. metadata
  :level: normal
  :topics: geometry, mesh
  :techniques: dialog, elements
"""
import pyformex as pf
from pyformex import _I, _C
from pyformex.elements import ElementType
_name = pf.Path(__file__).stem


eltypes = ElementType.list()
notype = '-----'
colors = ['black', 'blue', 'yellow', 'red']

def nextEltype(eltype):
    try:
        ind = eltypes.index(eltype) % len(eltypes)
    except IndexError:
        ind = 0
    return eltypes[ind]


def showElement(eltype, options):
    pf.clear()
    M = pf.Mesh(eltype=eltype)
    msg = f"Element type: {eltype}"

    if options['Show report']:
        print(M.report())

    ndim = M.level()

    if ndim == 3:
        pf.view('iso')
        # smoothwire()
    else:
        pf.view('front')
        # smoothwire()

    if options['Reverse']:
        M = M.reverse()
        msg += "\nReversed"

    i = 'xyz'.find(options['Mirror'])
    if i >= 0:
        M = M.trl(i, 0.2)
        M = M + M.reflect(i)
        msg += f"\nMirrored {i}"

    if options['Deform']:
        M.coords = M.coords.addNoise(rsize=0.2)
        # if options['Force dimensionality']:
        #     if ndim < 3:
        #         M.coords[..., 2] = 0.0
        #     if ndim < 2:
        #         M.coords[..., 1:] = 0.0
        msg += "\nDeformed"

    if options['Subdivide'] != 'No' and ndim > 0:
        if options['Subdivide'] == 'Uniform':
            ndiv = options['Subdivide_uni']
        else:
            ndiv = options['Subdivide_nonuni']
            # print(ndiv)
        ndiv = (ndiv,) * ndim
        # draw(M, color='yellow')
        M = M.subdivide(*ndiv, fuse=False)
        msg += f"\nSubdivided {ndiv}"

    totype = options['Convert to']
    if totype != notype:
        M = M.convert(totype)
        msg += f"\nConverted to {totype}"

    pf.drawText(msg, (10, pf.canvas.height()-20), size=18, color='black')

    M.setProp([5, 6])

    if options['Show nodes']:
        pf.draw(M.coords, marksize=8)
        pf.drawNumbers(M.coords, gravity='NE')
        if M.level() == 1:
            brd = M.borderNodes()
            pf.draw(M.coords[brd], marksize=10)

    if options['Draw as'] == 'Formex':
        M = M.toFormex()
    elif options['Draw as'] == 'Border' and ndim > 0:
        M = M.getBorderMesh()

    if options['Color setting'] == 'prop':
        pf.draw(M)
    else:
        pf.draw(M, color='red', bkcolor='blue')
    pf.zoom(1.2)

def showRes(res):
    eltype = res.pop('Element Type')
    if eltype == 'All':
        for el in eltypes:
            showElement(el, res)
    else:
        showElement(eltype, res)

def close():
    global dialog
    if dialog:
        dialog.close()
        dialog = None
    # Release script.ock
    pf.script.scriptRelease(__file__)

def show():
    if dialog.validate():
        showRes(dialog.results)

def showNext():
    if dialog.validate():
        res = dialog.results
        eltype = nextEltype(res['Element Type'])
        dialog.updateData({'Element Type': eltype})
        res['Element Type'] = eltype
        showRes(res)

def timeOut():
    show()
    pf.wait()
    close()

def change_eltype(item):
    eltype = item.value()
    dialog = item.dialog()
    if dialog:
        convert = dialog['Convert to']
        conversions = list(ElementType.get(eltype).conversions)
        convert.setChoices([notype] + conversions)
        convert.setValue(notype)

def run():
    global dialog
    dialog = pf.Dialog(caption=_name, store=_name+'_data', items=[
        _I('Element Type', choices=['All', ]+eltypes, func=change_eltype),
        _C('', [_I('Reverse', False, itemtype='bool'),]),
        _C('', [_I('Deform', False, itemtype='bool'),]),
        _I('Mirror', itemtype='radio', choices=['No', 'x', 'y', 'z']),
        _I('Subdivide', choices=['No', 'Uniform', 'Non-uniform']),
        _I('Subdivide_uni', 1, min=1),
        _I('Subdivide_nonuni', [0.0, 0.2, 0.5, 1.0]),
        _I('Convert to', choices=[notype] + eltypes),
        _I('Draw as', itemtype='radio', choices=['Mesh', 'Formex', 'Border']),
        _I('Show nodes', True),
        _I('Color setting', itemtype='radio', choices=['direct', 'prop']),
        _I('Show report', False, itemtype='bool'),
    ], enablers=[
        ('Subdivide', 'Uniform', 'Subdivide_uni'),
        ('Subdivide', 'Non-uniform', 'Subdivide_nonuni'),
    ], actions=[
        ('Close', close),
        ('Next', showNext),
        ('Show', show)],
    )
    dialog.show(timeoutfunc=timeOut)


if __name__ == '__draw__':
    run()

# End
