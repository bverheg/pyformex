#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""FontForge

This example demonstrates the use of FontForge library to render text. To be
able to run it, you need to have the FontForge library and its Python bindings
installed. On Debian GNU/Linux you can achieve this by installing the package
'python3-fontforge'.

See also: examples.Contour

.. metadata
  :level: advanced
  :topics: curve, font
  :techniques: bezier, borderfill
"""
import numpy as np
import pyformex as pf
from pyformex import _I
_name = pf.Path(__file__).stem

pf.utils.Module.require('fontforge')
import fontforge  # noqa: 402

debug_on = False

def intersection(self, other):
    """Find the intersection points of two plane curves"""
    # X = np.stack([self.coords, np.roll(self.coords, -1, axis=0)], axis=1)
    # print(X.shape)
    F = self.toMesh().toFormex()
    # create planes // z
    P = other.coords
    N = other.vectors().rotate(90)
    return pf.gt.intersectSegmentWithPlane(F, P, N)


def partitionByContour(self, contour, angle, sort):
    """Partition the surface by splitting it at a contour on the surface.

    """
    # edg = self.edges
    feat = self.featureEdges(angle=angle)
    p = self.maskedEdgeFrontWalk(mask=~feat, frontinc=0)
    if sort == 'number':
        p = pf.at.sortSubsets(p)
    elif sort == 'area':
        p = pf.at.sortSubsets(p, self.areas())
    return p


def glyphCurve(c):
    """Convert a glyph contour to a list of quad bezier curves."""
    if debug_on:
        print("Glyphcurve", c)
    # print([type(ci) for ci in c])
    points = []
    control = []
    P0 = c[0]
    points.append([P0.x, P0.y])
    for P in c[1:] + c[-1:]:
        if P0.on_curve and P.on_curve:
            if debug_on:
                print('straight segment')
            control.append([0.5*(P0.x+P.x), 0.5*(P0.y+P.y)])
            points.append([P.x, P.y])
            P0 = P
            continue
        elif P0.on_curve and not P.on_curve:
            if debug_on:
                print("undecided")
            P1 = P0
            P0 = P
            continue
        elif not P0.on_curve and P.on_curve:
            if debug_on:
                print("single quadratic segment")
            control.append([P0.x, P0.y])
            points.append([P.x, P.y])
            P0 = P
            continue
        else:  # not P0.on_curve and not P.on_curve:
            if debug_on:
                print("two quadratic segments")
            # central point to be interpolated
            PM = fontforge.point()
            PM.x = 0.5*(P0.x+P.x)
            PM.y = 0.5*(P0.y+P.y)
            PM.on_curve = True
            points.append([PM.x, PM.y])
            control.append([P0.x, P0.y])
            P1 = PM
            P0 = P
            continue

    return pf.Coords(points), pf.Coords(control)


def contourCurve(c):
    """Convert a fontforge contour to a pyFormex curve"""
    points, control = glyphCurve(c)
    # print(points)
    Q = pf.at.interleave(points, control)
    # print(Q)
    return pf.BezierSpline(control=Q, degree=2, closed=True)


def charContours(fontfile, character):
    font = fontforge.open(fontfile, 5)
    if debug_on:
        print("FONT INFO: %s" % font)
    # print(dir(font))
    # print(font.gpos_lookups)
    g = font[ord(character)]
    if debug_on:
        print("GLYPH INFO: %s" % g)
    # print(dir(g))
    # print(g.getPosSub)
    l = g.layers[1]
    print("Number of curves: %s" % len(l))
    # c = l[0]
    # print(c)
    # print(dir(c))
    # print(c.closed)
    # print(c.is_quadratic)
    # print(c.isClockwise())
    # print(len(c))
    # print(c.reverseDirection())
    # if c.isClockwise():
    #     c = c.reverseDirection()
    return l


def connect2curves(c0, c1):
    x0 = c0.coords
    x1 = c1.coords
    i, j, d = pf.gt.closestPair(x0, x1)
    x = np.concatenate([np.roll(x0, -i, axis=0), np.roll(x1, -j, axis=0)])
    return pf.BezierSpline(control=x, degree=2, closed=True)


def charCurves(fontfile, character):
    l = charContours(fontfile, character)
    c = [contourCurve(li) for li in l]
    fontname = pf.utils.projectName(fontfile)
    pf.PF[f"{fontname}-{character}"] = c
    return c


def drawCurve(curve, color, fill=None, with_border=True, with_points=True):
    # print("CURVE", curve)
    if fill is not None:
        border = curve.approx()
        if with_border:
            pf.draw(border, color='red')
        pf.drawNumbers(border.coords, color='red')
        pf.clear()
        pf.draw(border)
        # t,x,wl,wt = intersection(P,P)
        # print(x.shape)
        # draw(Formex(x),color=red)
        # return
        if fill in ('planar', 'radial', 'border'):
            print(f"fillBorder {fill}")
            surface = pf.fillBorder(border, fill)
        else:
            from pyformex.plugins.gts_itf import gtsdelaunay
            try:
                surface = gtsdelaunay(border.coords)
            except Exception:
                # print(sys.path)
                pf.warning("DELAUNAY fill failed")
                surface = []
        pf.draw(surface, color=color)
        # drawNumbers(surface)
    else:
        pf.draw(curve, color=color)
    if with_points:
        pf.drawNumbers(curve.pointsOn())
        pf.drawNumbers(curve.pointsOff(), color='red')


def drawCurve2(curve, color, fill=None, with_border=True, with_points=True):
    if fill:
        curve = connect2curves(*curve)
        drawCurve(curve, 'blue', fill)
    else:
        drawCurve(curve[0], color, with_border=with_border, with_points=with_points)
        drawCurve(curve[1], color, with_border=with_border, with_points=with_points)


def show(fontname, character, fill=None):
    """Show a character from a font"""
    print("Char '%s' from font '%s'" % (character, fontname))
    curve = charCurves(fontname, character)
    # size = curve[0].pointsOn().bbox().dsize()
    pf.clear()

    if fill:
        ncurv = len(curve)
        print(f"# curves: {ncurv}")
        if ncurv == 1:
            drawCurve(curve[0], 'blue', fill=fill)
        elif ncurv == 2:
            drawCurve2(curve, 'blue', fill=fill)
    else:
        for c in curve:
            drawCurve(c, 'blue')
    pf.zoomAll()


# Initialization

# List font files on nonstandard places
my_fonts = (
    pf.cfg['datadir'] / 'blippok.ttf',
)
fonts = [f for f in my_fonts if f.exists()] + pf.utils.listFonts()
print(f"There are {len(fonts)} fonts",
      f"The default font is {fonts[0]}",
      sep='\n')

store = _name + '_data'
pf.PF.setdefault(store, {
    'fontname': fonts[0],
    'character': 'S',
})

def run():
    global debug_on
    choices = ['planar', 'radial', 'border', 'delaunay', 'none']
    import random
    random.shuffle(choices)
    res = pf.askItems(caption=_name, store=_name+'_data', items=[
        _I('fontname', choices=fonts),
        _I('character', max=1),
        _I('fill', itemtype='combo', choices=choices),
        _I('debug', False),
    ])
    if res:
        debug_on = res.pop('debug')
        if res['fill'] == 'None':
            del res['fill']
        show(**res)


if __name__ == '__draw__':
    run()


# End
