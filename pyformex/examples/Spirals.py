#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Spirals

This example shows how to create a spiral curve and how to spread points
evenly along a curve.

See also the Sweep example for a more sophisticated application of spirals.

.. metadata
  :level: normal
  :topics: geometry, curve
  :techniques: transform, spiral
"""
import numpy as np
import pyformex as pf
_name = pf.Path(__file__).stem


def createLine(nseg, turns):
    """Create a line along x-as with nseg segments and length nturns*360"""
    return pf.PolyLine(pf.simple.grid1(nseg+1).scale(turns*2*np.pi/nseg))

def createSpiral(nseg=100, turns=1., phi=30., alpha=70., c=0.):
    X = pf.simple.grid1(nseg+1).scale(turns*2*np.pi/nseg)
    a = pf.tand(phi)
    b = pf.tand(phi) / pf.tand(alpha)
    zf = lambda x: c * np.exp(b*x)
    rf = lambda x: a * np.exp(b*x)
    Y = X.spiral((1, 0, 2), rfunc=rf, zfunc=zf, angle_spec=pf.RAD)
    return pf.PolyLine(Y)

def run():
    pf.linewidth(2)
    pf.clear()
    pf.flat()
    pf.view('front')
    turns = 1.   # Number of turns of the spiral
    nseg = 36    # number of segments over one turn
    L = createLine(nseg, turns)
    S = createSpiral(nseg, turns)
    pf.draw(L.coords, color='blue')
    pf.drawNumbers(L.coords, color='blue', gravity='se')
    pf.draw(S, color='red')
    pf.draw(S.coords, color='red')
    pf.drawNumbers(S.coords, color='red', gravity='ne')
    pf.zoomAll()


if __name__ == '__draw__':
    run()

# End
