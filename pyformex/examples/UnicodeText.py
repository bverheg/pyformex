#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""UnicodeText

This example illustrates how to draw unicode text. The standard TextureFonts
delivered with pyFormex contain only the ASCII characters 32..128. However,
the :class:`FontTexture` class supports creating textures for any unicode
character included in the (monospace) fonts on your system. You can create
a specialized FontTexture on the fly, as illustrated in this example.

.. metadata
  :level: normal
  :topics: text
  :techniques: unicode, fonttexture
"""
import numpy as np
import pyformex as pf
_name = pf.Path(__file__).stem


def run():
    pf.clear()
    text = 'abç°ë€→☺\N{cancer}'
    pf.drawText(text, pos=(200,200), font=pf.FontTexture(charlist=text))
    pf.drawText(text, pos=(200,100), size=28,
                font=pf.FontTexture(size=28, charlist=text))
    pf.drawText(text, pos=(0,0,0), size=28,
                font=pf.FontTexture(size=28, charlist=text))


if __name__ == '__draw__':
    run()

# End
