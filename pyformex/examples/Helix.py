#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Helix

This is basically the same example as Torus.py, but it constructs the
resulting geometry step by step, making it suited for a first time
initiation.

.. metadata
  :level: beginner
  :topics: geometry
  :techniques: color
"""
import pyformex as pf
_name = pf.Path(__file__).stem


def run():
    m = 36  # number of cells along helix
    n = 10  # number of cells along circular cross section
    pf.reset()
    pf.view('xy')
    pf.delay(2)
    pf.setDrawOptions({'clear': True})
    pf.bgcolor('white')
    # A triangle in x-y plane
    pf.draw(F:=pf.Formex('l:164', [1, 2, 3]))
    # replicate in direction x
    pf.draw(F:=F.replic(m, 1., 0))
    # replicate in direction y
    pf.draw(F:=F.replic(n, 1., 1))
    # translate to prepare cylindrical transform
    pf.draw(F:=F.translate(2, 1.), view='iso')
    # apply cylindrical transform
    pf.draw(F:=F.cylindrical([2, 1, 0], [1., 360./n, 1.]))
    # replicate in direction of the cylinder axis
    pf.draw(F:=F.replic(5, m*1., 2))
    # rotate so that it is skew wrt. axes
    pf.draw(F:=F.rotate(-10., 0))
    # translate to prepare cylindrical transform
    pf.draw(F:=F.translate(0, 5.))
    # apply another cylindrical transform
    pf.draw(F:=F.cylindrical([0, 2, 1], [1., 360./m, 1.]))
    # show from another angle
    pf.draw(F, view='-zy')


if __name__ == '__draw__':
    run()

# End
