#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""ClippingBox

Clips a mesh with sheared box without using VTK.

.. metadata
  :level: normal
  :topics: surface
  :techniques: boolean, partition
"""
import pyformex as pf
from pyformex import _I
_name = pf.Path(__file__).stem


def run():
    pf.clear()
    pf.smooth()

    # Create two surfaces
    S1 = pf.simple.sphere(10).setProp(1)
    S2 = pf.simple.cuboid(*S1.bbox()).toMesh().scale(
        [0.8, 0.8, 1]).rot(30, 1).rot(20, 0).shear(
            2, 1, 0.3).toSurface().setProp(6)
    pf.draw(S1, alpha=1)
    pf.draw(S2)

    # Create all the set operations
    S = S1.gts_set(S2, 'a', prop=[1, 1, 6, 6], verbose=True)

    # Collect everything in a nice dialog to present
    result = dict(zip(
        ['s1 && s2', 's1', 's2', 's1 in s2', 's1 out s2', 's2 in s1',
         's2 out s1', 's1 + s2', 's1 * s2', 's1 - s2', 's2 - s1', 'symdiff'],
        [S1 + S2, S1, S2] + S))

    def show(item):
        key = item.value()
        pf.clear()
        if key in result:
            pf.draw(result[key])

    dialog = pf.Dialog(caption="ClippingBox", items=[
        _I('info', "Which surface to display?", itemtype='label', text=''),
        _I('key', choices=list(result.keys()), itemtype='hpush', func=show,
           text=''),
    ], actions=[('Cancel',)])
    dialog.show()


if __name__ == '__draw__':
    run()
