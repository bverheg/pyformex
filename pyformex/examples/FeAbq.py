#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""FeAbq

.. metadata
  :level: advanced
  :topics: FEA
  :techniques: persistence, dialog, color
"""
import numpy as np
import pyformex as pf
from pyformex.fe import FEModel, PropertyDB, ElemSection, Eset
from pyformex.fe.fe_abq import Step, Output, Result, AbqData
_name = pf.Path(__file__).stem


def base(nplex):
    """Return the pattern string to generate suqare base cells"""
    return {
        3: '3:012934',
        4: '4:0123',
    }[nplex]


def abq_eltype(nplex):
    """Return matching abaqus/calculix eltype for given pyFormex plexitude"""
    return {
        3: 'CPS3',
        4: 'CPS4',
        8: 'CPS8',
    }[nplex]


def printModel(M):
    """print the model M"""
    print("===================\nMERGED MODEL")
    print("NODES")
    print(M.coords)
    for i, e in enumerate(M.elems):
        print("PART %s" %i)
        print(e)
    print("===================")


def drawFEModel(M, nodes=True, elems=True, nodenrs=True, elemnrs=True):
    """draw the model M"""
    pf.clear()
    if nodes or nodenrs:
        if nodes:
            pf.draw(M.coords)
        if nodenrs:
            pf.drawNumbers(M.coords)
    if elems or elemnrs:
        meshes = [pf.Mesh(M.coords, e, prop=i+1) for i, e in enumerate(M.elems)]
        if elems:
            pf.draw(meshes)
        if elemnrs:
            [pf.drawNumbers(i, color='red') for i in meshes]
    pf.zoomAll()


def run():
    global parts, M, F

    na, ma = 4, 3  # Size of domain A
    nb, mb = 3, 4  # size of domain B
    # Make sure the property numbers never clash!
    pa, pb, pc = 3, 4, 5  # Properties corresponding to domains A,B,C
    pb1 = 2  # Property for part of domain B
    pbc, pld = 1, 6  # Properties corresponding to boundary/loaded nodes

    _choices = ["A mixture of tri3 and quad4", "Only quad4", "Only quad8"]
    ans = _choices.index(pf.ask("What kind of elements do you want?", _choices))
    if ans == 0:
        baseA = pf.Formex(base(3))
        baseB = pf.Formex(base(4))
    else:
        baseA = pf.Formex(base(4))
        baseB = pf.Formex(base(4))

    A = baseA.replicm((na, ma)).setProp(pa)
    B = baseB.replicm((nb, mb)).translate([na, 0, 0]).setProp(pb)
    # Change every second element of B to property pb1
    B.prop[np.arange(B.prop.size) % 2 == 1] = pb1
    print(B.prop)
    C = A.rotate(90).setProp(pc)
    parts = [A, B, C]
    pf.draw(parts)

    # First convert parts to Meshes
    parts = [p.toMesh() for p in parts]
    if ans == 2:
        # Convert meshes to 'quad8'
        parts = [p.convert('quad8') for p in parts]

    # Create the finite element model
    # A model contains multiple Meshes with a single merged set of nodes
    M = FEModel(parts)
    # drawFEModel(M)
    # return

    # Transfer the properties from the parts in a global set
    elemprops = np.concatenate([part.prop for part in parts])

    # Create the property database
    P = PropertyDB()
    # In this pf.simple.example, we do not load a material/section database,
    # but define the data directly
    steel = {
        'name': 'steel',
        'young_modulus': 207000,
        'poisson_ratio': 0.3,
        'density': 7.85e-9,  # Not Used, but Abaqus does not like a material without
    }
    thin_plate = {
        'name': 'thin_plate',
        'sectiontype': 'solid',
        'thickness': 0.01,
        'material': 'steel',
    }
    medium_plate = {
        'name': 'thin_plate',
        'sectiontype': 'solid',
        'thickness': 0.015,
        'material': 'steel',
    }
    thick_plate = {
        'name': 'thick_plate',
        'sectiontype': 'solid',
        'thickness': 0.02,
        'material': 'steel',
    }
    print(thin_plate)
    print(medium_plate)
    print(thick_plate)

    # Create element sets according to the properties pa,pb,pb1,pc:
    esets = dict([(v, np.where(elemprops==v)[0]) for v in [pa, pb, pb1, pc]])

    # Set the element properties
    ea, eb, ec = [abq_eltype(p.nplex()) for p in parts]
    P.elemProp(set=esets[pa], eltype=ea,
               section=ElemSection(section=thin_plate, material=steel))
    P.elemProp(set=esets[pb], eltype=eb,
               section=ElemSection(section=thick_plate, material=steel))
    P.elemProp(set=esets[pb1], eltype=eb,
               section=ElemSection(section=thick_plate, material=steel))
    P.elemProp(set=esets[pc], eltype=ec,
               section=ElemSection(section=medium_plate, material=steel))

    print("Element properties")
    for p in P.getProp('e'):
        print(p)

    # Set the nodal properties
    xmin, xmax = M.coords.bbox()[:, 0]
    bnodes = np.where(M.coords.test(min=xmax-0.01))[0]  # Right end nodes
    lnodes = np.where(M.coords.test(max=xmin+0.01))[0]  # Left end nodes

    print("Boundary nodes: %s" % bnodes)
    print("Loaded nodes: %s" % lnodes)

    P.nodeProp(tag='init', set=bnodes, bound=[1, 1, 0, 0, 0, 0])
    P.nodeProp(tag='step1', set=lnodes, name='Loaded', cload=[-10., 0., 0., 0., 0., 0.])
    P.nodeProp(tag='step2', set='Loaded', cload=[-10., 10., 0., 0., 0., 0.])

    # Coloring the nodes gets easier using a Formex
    F = pf.Formex(M.coords)
    F.setProp(0)
    F.prop[bnodes] = pbc
    F.prop[lnodes] = pld

    print("Node properties")
    for p in P.getProp('n'):
        print(p)

    while pf.ack("Renumber nodes?"):
        # renumber the nodes randomly
        print([e.eltype for e in M.elems])
        old, new = M.renumber()
        print(old, new)
        print([e for e in M.elems])
        print([e.eltype for e in M.elems])
        drawFEModel(M)
        if pf.widgets.input_timeout > 0:
            break

    # Request default output plus output of S in elements of part B.
    # If the abqdata are written with group_by_group==True (see at bottom),
    # all elements of each group in elems will be collected in a set named
    # Eset('grp',index), where index is the index of the group in the elems list.
    # Thus, all elements of part B will end up in Eset('grp',1)
    out = [Output(type='history'),
           Output(type='field'),
           Output(type='field', kind='element', set=Eset('grp', 1), vars=['S']),
           ]

    # Create requests for output to the .fil file.
    # - the displacements in all nodes
    # - the stress components in all elements
    # - the stresses averaged at the nodes
    # - the principal stresses and stress invariants in the elements of part B.
    # (add output='PRINT' to get the results printed in the .dat file)
    res = [Result(kind='NODE', keys=['U']),
           Result(kind='ELEMENT', keys=['S']),
           Result(kind='ELEMENT', keys=['S'], pos='AVERAGED AT NODES'),
           Result(kind='ELEMENT', keys=['SP', 'SINV'], set=Eset('grp', 1)),
           ]

    # Define steps (default is static)
    step1 = Step(time=[1., 1., 0.01, 1.], tags=['step1'], out=out, res=res)
    step2 = Step(time=[1., 1., 0.01, 1.], tags=['step2'], out=out, res=res)

    # collect all data
    #
    # !! currently output/result request are global to all steps
    # !! this will be changed in future
    #
    data = AbqData(M, prop=P, steps=[step1, step2], initial=['init'])

    if pf.ack('Export this model in ABAQUS input format?', default='No'):
        fn = pf.askFilename(filter='inp')
        if fn:
            data.write(jobname=fn, group_by_group=True)


# Initialize
pf.smoothwire()
pf.lights(False)
pf.transparent()
pf.clear()

if __name__ == '__draw__':
    run()

# End
