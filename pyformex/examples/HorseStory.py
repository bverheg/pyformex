#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Horse

This script reads horse.pgf, transforms it into a surface,
loads the surface plugin and cuts the horse in a number of surfaces.

.. metadata
  :level: normal
  :topics: surface
  :techniques: animation, colors
"""
import numpy as np
import pyformex as pf
_name = pf.Path(__file__).stem

def run():
    global y
    pf.reset()
    pf.smooth()

    x = 20
    y = pf.canvas.height()-100

    def say(text):
        global y
        pf.drawText(text, (x, y))
        y -=20

    print('Click Step to continue')

    say('A Horse Story...')
    y -= 10
    F = pf.Formex.read(pf.cfg['datadir'] / 'horse.pgf')
    A = pf.draw(F)
    pf.pause()

    say('It\'s rather sad, but')
    pf.pause()


    say('the horse was badly cut;')
    T = F.cutWithPlane([0., 0., 0.], [-1., 0., 0.], '+')
    pf.undraw(A)
    A = pf.draw(T)
    pf.pause()


    say('to keep it stable,')
    pf.undraw(A)
    A = pf.draw(T.rotate(-80))
    pf.pause()


    say('the doctors were able')
    pf.undraw(A)
    A = pf.draw(T)
    pf.pause()


    say('to add a mirrored clone:')
    T += T.reflect(0)
    pf.undraw(A)
    A = pf.draw(T)
    pf.pause()

    say('A method as yet unknown!')
    colors = 0.5 * np.random.random((10, 3))
    for color in colors:
        B = pf.draw(T, color=color)
        pf.undraw(A)
        A = B
        pf.sleep(0.5)


if __name__ == '__draw__':
    run()

# End
