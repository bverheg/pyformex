#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Carpetry

This example illustrates the use of the Mesh conversion techniques and the
creation of colored value plots on surfaces.

.. metadata
  :level: normal
  :topics: mesh, illustration, surface
  :techniques: color, random, image, movie, extrude
"""
import random
import pyformex as pf
from pyformex import _I
from pyformex.gui.menus import Surface
_name = pf.Path(__file__).stem


def atExit():
    pf.cfg['gui/autozoomfactor'] = saved_autozoomfactor


def drawMesh(M):
    pf.clear()
    pf.draw(M)
    pf.drawText("%s %s elements" % (M.nelems(), M.elName()), (20, 20), size=20)

def run():
    # make sure this is a good aspect ratio if you want a movie
    nx, ny = 4, 3
    global saved_autozoomfactor
    saved_autozoomfactor = pf.cfg['gui/autozoomfactor']

    pf.cfg['gui/autozoomfactor'] = 2.0

    pf.clear()
    pf.view('front')
    pf.smooth()
    pf.transparent()
    pf.linewidth(1)

    M = pf.Mesh(eltype='quad4').subdivide(nx, ny).scale([nx, ny, 0.]).setProp(1)

    V = Surface.SelectableStatsValues
    possible_keys = [k for k in V.keys() if not V[k][1]][:-1]
    nkeys = len(possible_keys)

    maxconv = 5  # inclusive for Python's randint !
    minconv = 3
    minelems = 6000
    maxelems = 20000

    save = False

    def carpet(M):
        conversions = []
        nconv = random.randint(minconv, maxconv)
        while (len(conversions) < nconv and M.nelems() < maxelems
               or M.nelems() < minelems):
            possible_conversions = list(M.eltype.conversions)
            conv = random.choice(possible_conversions)
            conversions.append(conv)
            M = M.convert(conv, fuse=False)

        if M.elName() != 'tri3':
            M = M.convert('tri3', fuse=False)
            conversions.append('tri3')

        M = M.fuse().compact()
        print(f"{M.nelems()} patches")
        print(f"{conversions=}")
        # Coloring
        key = possible_keys[random.randint(0, nkeys-1)]
        print(f"colored by {key}")
        func = V[key][0]
        S = pf.TriSurface(M)
        val = func(S)
        pf.PF['surface'] = S
        pm = pf.pmgr()
        pm.selection = ['surface']
        Surface.showSurfaceValue(S, str(conversions), val, False, legend=False)
        # for a in pf.canvas.scene.decorations+pf.canvas.scene.annotations:
        #     pf.undraw(a)

    pf.clear()
    pf.flat()
    pf.lights(True)
    pf.transparent(False)

    if pf.interactive:
        pf.canvasSize(nx*200, ny*200)
        # canvasSize(720,576)
        print("running interactively")
        print(pf.Path.cwd())
        res = pf.askItems([
            _I('n', 1, text='Number of carpets'),
            _I('save', False, text='Save images'),
        ])
        if not res:
            return

        n = res['n']
        save = res['save']
        if save:
            pf.saveImage(filename='Carpetry-000.jpg', multi=True,
                         hotkey=False, autosave=False, quality=95,
                         verbose=False)

        A = None
        for i in range(n):
            with pf.busyCursor():
                carpet(M)
            B = pf.canvas.actors[-1:]
            if A:
                pf.undraw(A)
            A = B
            if save:
                pf.gui.image.saveNext()

        if save:
            pf.saveImage()  # turn off multisave mode
            # files = pf.utils.globFiles(image.multisave['filename'].glob())
            # image.createMovie(files, encoder='convert', delay=1, colors=256)

    else:
        pf.canvasSize(nx*200, ny*200)
        print("just saving image")
        with pf.busyCursor():
            carpet(M)
        pf.saveImage('testje2.png')
        # return(all=True)
        pf.quitGUI()   # ?? pf.GUI.close() ?


if __name__ == '__draw__':
    run()

# End
