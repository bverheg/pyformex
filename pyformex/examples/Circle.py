#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Circle

.. metadata
  :level: normal
  :topics: geometry
  :techniques: connect, dialog, animation
"""
import numpy as np
import pyformex as pf
from pyformex import _I
_name = pf.Path(__file__).stem
circle = pf.simple.circle

def run():
    pf.resetAll()
    pf.delay(1)
    C = pf.draw(circle(1., 1.), color='red', sticky=True)
    pf.lockCamera()
    for i in [3, 4, 5, 6, 8, 12, 20, 60, 180]:
        pf.draw(circle(360./i, 360./i), clear=True, linewidth=3)
        pf.draw(circle(360./i, 2*360./i), clear=True, linewidth=3)
        pf.draw(circle(360./i, 360./i, 180.), clear=True, linewidth=3)
    pf.unlockCamera()

    # Example of the use
    pf.clear(sticky=True)
    n = 40
    h = 0.5
    line = pf.Formex('l:'+'1'*n).scale(2./n).translate([-1., 0., 0.])
    curve = line.bump(1, [0., h, 0.], lambda x: 1.-x**2)
    curve.setProp(1)
    pf.draw(line)
    pf.draw(curve)


    # Create circles in the middle of each segment, with normal along the segment
    # begin and end points of segment
    A = curve.coords[:, 0, :]
    B = curve.coords[:, 1, :]
    # midpoint and segment director
    C = 0.5*(B+A)
    D = B-A
    # vector initially normal to circle defined above
    nuc = np.array([0., 0., 1.])
    # rotation angles and vectors
    ang, rot = pf.gt.rotationAngle(nuc, D)
    # diameters varying linearly with the |x| coordinate
    diam = 0.1*h*(2.-abs(C[:, 0]))
    # finally, here are the circles:
    circles = [circle().scale(d).rotate(a, r).translate(c)
               for d, r, a, c in zip(diam, rot, ang, C)]
    F = pf.Formex.concatenate(circles).setProp(3)
    pf.draw(F)

    # And now something more fancy: connect 1 out of 15 points of the circles

    res = pf.askItems([
        _I('Connect circles', True),
        _I('Create Triangles', True),
        _I('Fly Through', True),
    ])

    if res:

        if res['Connect circles'] or res['Create Triangles']:
            conn = np.arange(0, 180, 15)

        if res['Connect circles']:
            G = pf.Formex.concatenate([
                pf.connect([c1.select(conn), c2.select(conn)])
                for c1, c2 in zip(circles[:-1], circles[1:])])
            pf.draw(G)

        if res['Create Triangles']:
            conn1 = np.concatenate([conn[1:], conn[:1]])
            G = pf.Formex.concatenate([
                pf.connect([c1.select(conn), c2.select(conn), c2.select(conn1)])
                for c1, c2 in zip(circles[:-1], circles[1:])])
            pf.smooth()
            pf.draw(G)

        if res['Fly Through']:
            pf.flyAlong(curve, sleeptime=0.1)
            pf.clear()
            pf.flat()
            pf.draw(line)
            pf.draw(curve)
            pf.draw(F)


if __name__ == '__draw__':
    run()
# End
