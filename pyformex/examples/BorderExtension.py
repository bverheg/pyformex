#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""BorderExtension

This example shows how to create extension tubes on the borders of a
surface techniques.

.. metadata
  :level: normal
  :topics: surface
  :techniques: extrude, borderfill, cut
"""
import numpy as np
import pyformex as pf
_name = pf.Path(__file__).stem

at = pf.at
gt = pf.geomtools

def run():
    pf.reset()
    pf.clear()
    # read and draw the surface
    pf.chdir(pf.cfg['datadir'])
    S = pf.TriSurface.read('bifurcation.off.gz').smooth()
    pf.draw(S)
    # Get the center point and the border curves
    CS = S.center()
    border = S.borderMeshes()

    BL = []
    for B in border:
        pf.draw(B, color='red', linewidth=2)
        # find the smallest direction of the curve
        d, s = gt.smallestDirection(B.coords, return_size=True)
        # Find outbound direction of extension
        CB = B.center()
        p = np.sign(at.projection((CB-CS), d))
        # Flatten the border curve and translate it outward
        B1 = B.projectOnPlane(d, CB).trl(d, s*8*p)
        pf.draw(B1, color='green')
        # create a surface between border curve and translated flat curve
        M = B.connect(B1)
        pf.draw(M, color='blue', bkcolor='yellow')
        BL.append(M)

    pf.zoomAll()

    if pf.ack("Convert extensions to 'tri3' and add to surface?"):
        # convert extensions to 'tri3'
        BL = [B.setProp(i+1).convert('tri3') for i, B in enumerate(BL)]

        pf.clear()
        pf.draw(BL, color='yellow', bkcolor='green')
        pf.PF.update(dict((f'BE-{i}', B) for i, B in enumerate(BL)))

        T = pf.TriSurface.concatenate([S]+BL).fuse().compact().fixNormals('internal')
        pf.clear()
        pf.draw(T)
        pf.PF.update({'T': T})


if __name__ == '__draw__':
    run()

# End
