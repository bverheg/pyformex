#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Bezier

The example shows how to define and draw a parametric curve.
A Bezier curve is defined by its two end points and two intermediate control
points. For drawing, an approximation is constructed by computing a high
number of on-curve points and connecting them with straight segments.
Four different curves and there end tangents are shown, all the same end points
but different internal control points.

See also example BezierCurve for the use of a built-in function to compute
points on a Bezier Curve.

.. metadata
  :level: beginner
  :topics: geometry, curve
  :techniques: connect, color, solve
"""
import numpy as np
import pyformex as pf
_name = pf.Path(__file__).stem


def build_matrix(atoms, vars):
    """Build a matrix of functions of coords.

    atoms is a list of text strings each representing a function of variables
    defined in vars.
    vars is a dictionary where the keys are variable names appearing in atoms
    and the values are 1-dim arrays of values. All variables should have the
    same shape !
    A matrix is returned where each row contains the values of atoms evaluated
    for one set of the variables.
    """
    keys = list(vars)
    nval = len(vars[keys[0]])
    aa = np.zeros((nval, len(atoms)), pf.Float)
    for k, a in enumerate(atoms):
        aa[:, k] = eval(a, vars)
    return aa


class Bezier():
    """A class representing a Bezier curve"""

    atoms = {
        1: ('1-t', 't'),
        2: ('(1-t)**2', '2*t*(1-t)', 't**2'),
        3: ('(1-t)**3', '3*t*(1-t)**2', '3*t**2*(1-t)', 't**3'),
    }

    def __init__(self, pts):
        """Create a bezier curve.

        pts is an Coords array with at least 2 points.
        A Bezier curve of degree npts-1 is constructed between the first
        and the last points.
        """
        self.pts = pts

    def at(self, t):
        """Returns the points of the curve for parameter values t"""
        deg = self.pts.shape[0] - 1
        aa = build_matrix(Bezier.atoms[deg], {'t': t})
        return np.dot(aa, self.pts)


def drawNumberedPoints(x, color):
    """Draw numbered point in color"""
    pf.draw(x, color=color)
    pf.drawNumbers(x, color=color)

def run():
    pf.resetAll()
    n = 100  # number of segments to draw along the curve
    t = np.arange(n+1)/float(n)  # parametric values for the curve approximation
    # create and draw 4 curves
    for d in np.arange(4) * 0.2:
        # the control points
        x = pf.Coords([[0., 0.], [1./3., d], [2./3., 4*d**2], [1., 0.]])
        drawNumberedPoints(x, 'red')
        # the end tangents
        H = pf.Formex(x.reshape(2, 2, 3))
        pf.draw(H, color='red')
        # the curve
        curve = Bezier(x)
        F = pf.Formex(curve.at(t))  # points on the curve
        G = pf.connect([F, F], bias=[0, 1])  # curve approximation
        pf.draw(G)


if __name__ == '__draw__':
    run()
# End
