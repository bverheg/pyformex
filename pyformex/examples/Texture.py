#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Texture

Shows how to draw with textures and how to set a background image.

.. metadata
  :level: normal
  :topics: Image, Geometry
  :techniques: texture
"""
import numpy as np
import pyformex as pf
_name = pf.Path(__file__).stem


def run():
    pf.clear()
    pf.smooth()

    image = pf.cfg['pyformexdir'] / 'data' / 'butterfly.png'

    F = pf.simple.cuboid().centered().toMesh().getBorderMesh()
    F.attrib(color='white')
    G = pf.Formex('4:0123')
    H = G.replicm((3, 2)).toMesh().setProp(np.arange(1, 7)).centered()
    K = G.scale(200).toMesh()
    K.attrib(color='yellow', rendertype=2, texture=image)
    pf.draw([F, H, K], texture=image)
    pf.drawText(image, (200, 20), size=20)

    pf.view('iso')
    pf.zoomAll()
    pf.zoom(0.5)

    pf.bgcolor(color=['white', 'yellow', 'yellow', 'white'], image=image)


if __name__ == '__draw__':
    run()

# End
