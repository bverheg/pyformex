#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Seed

Graphical feedback to the creeation of non-uniform seeds.

.. metadata
  :level: normal
  :topics: meshing
  :techniques: seed, dialog, fslider, undraw
"""
import pyformex as pf
from pyformex import _I
_name = pf.Path(__file__).stem


# globals
base = None
seed_ticks = None
x = None
SA = None
dialog = None
saved_e = None



def create_globals(n):
    """Create globals"""
    global base, seed_ticks, x
    baseline = pf.Formex('l:1')
    ticks = pf.Formex('l:4').scale(0.1).replicate(n+1, step=1./n)
    base = pf.List([baseline, ticks])
    seed_ticks = pf.Formex('l:2').scale(0.1).replicate(n+1, step=1./n)
    x = pf.at.unitDivisor(n)
    show_base()


def show_base():
    """Draw the fixed base and lock camera"""
    pf.unlockCamera()
    pf.clear()
    pf.linewidth(2)
    pf.view('front')
    pf.draw(base)
    pf.lockCamera()


def redraw(e0, e1=None):
    """Redraw seeds with new value for e0,e1"""
    global SA, saved_e
    if e1 is None:
        e1 = e0
    saved_e = (e0, e1)
    seeds = pf.at.unitAttractor(x, e0, e1)
    print("e0=%s, e1=%s: %s" % (e0, e1, seeds))
    seed_ticks.coords.x = seeds.reshape(-1, 1)
    SB = pf.draw(seed_ticks, color='red')
    pf.undraw(SA)
    SA = SB


def do_slider(fld):
    fld.setEnabled(False)  # Avoid a stream of events we can't handle
    if fld.name() == 'e0':
        redraw(fld.value(), saved_e[1])
    elif fld.name() == 'e1':
        redraw(saved_e[0], fld.value())
    fld.setEnabled(True)


def show():
    """Accept data, redraw seeds"""
    if dialog.validate():
        res = dialog.results
        ndiv = res['ndiv']
        if ndiv != len(x) + 1:
            create_globals(ndiv)
        e0 = res['e0']
        e1 = res['e1']
        redraw(e0, e1)


def close():
    global dialog
    if dialog:
        dialog.close()
        dialog = None
    pf.unlockCamera()
    print("Releasing lock")
    pf.script.scriptRelease(__file__)


def timeOut():
    try:
        show()
    finally:
        close()


def create_dialog():
    global dialog
    if dialog is not None:
        return

    dialog = pf.Dialog(caption='Seed parameters', items=[
        _I('ndiv', len(x)-1, tooltip='Number of intervals'),
        _I('e0', saved_e[0], itemtype='fslider', min=-1.0, max=1.0, dec=1,
           scale=0.02, func=do_slider, tooltip='Attractor force to 0.0'),
        _I('e1', saved_e[1], itemtype='fslider', min=-1.0, max=1.0, dec=1,
           scale=0.02, func=do_slider, tooltip='Attractor force to 1.0'),
    ], actions=[('Close', close), ('Show', show)], default='Show',)
    # Always install a timeout in official examples!
    dialog.show(timeoutfunc=timeOut)


def run():
    """Show the uniform seeds and the seed dialog"""
    create_globals(10)
    redraw(0., 0.)
    create_dialog()
    pf.script.scriptLock(__file__)


if __name__ == '__draw__':
    run()

# End
