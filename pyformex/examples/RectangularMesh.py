#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##

"""RectangularMesh

This example illustrates the use of the mesh.rectangle function
and the use of seeds in generating Meshes.

.. metadata
  :level: beginner
  :topics: geometry, mesh
  :techniques: rectangular, seed, layout
"""
import pyformex as pf
_name = pf.Path(__file__).stem


def run():
    """Main function.

    This is executed on each run.
    """
    pf.clearAll()
    pf.smoothwire()
    pf.fgcolor('red')
    pf.layout(6, 3)

    # TODO: initialize viewport settings from previous
    # Viewports are not correctly initialized!
    # we need to reset canvas settings
    pf.viewport(0)
    pf.smoothwire()
    pf.fgcolor('red')
    M = pf.mesh.rectangle(1., 1., 6, 3)
    pf.draw(M)

    pf.viewport(1)
    M = pf.mesh.rectangle(1., 1., (6, 0.3), (6, 0.3))
    pf.draw(M)

    pf.viewport(2)
    pf.smoothwire()
    pf.fgcolor('red')
    M = pf.mesh.rectangle(1., 1., (6, 0.3), (6, -0.5))
    pf.draw(M)

    pf.viewport(3)
    pf.smoothwire()
    pf.fgcolor('red')
    M = pf.mesh.rectangle(1., 1., (6, 0.5, 0.5), (6, 1, 1))
    pf.draw(M)

    pf.viewport(4)
    pf.smoothwire()
    pf.fgcolor('red')
    M = pf.mesh.rectangle(1., 1., (6, -1, -1), (6, -1, -1))
    pf.draw(M)

    pf.viewport(5)
    pf.smoothwire()
    pf.fgcolor('red')
    M = pf.mesh.rectangle(2., 1., (6, -1, -1), (6, -1, -1))
    pf.draw(M)


if __name__ == '__draw__':
    run()

# End
