#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
#
"""FieldDraw

This example demonstrates how to use field variables and how to render them.

.. metadata
  :level: advanced
  :topics: mesh, postprocess
  :techniques: field, color
"""
import pyformex as pf
_name = pf.Path(__file__).stem


def run():
    pf.clear()
    pf.view('front')
    pf.smoothwire()
    pf.layout(4, 2)
    F = pf.simple.rectangle(4, 3)
    M = F.toMesh()
    # M.attrib(color=yellow)
    # add some fields

    # 1. distance from point 7, defined at nodes
    data = pf.at.length(M.coords - M.coords[7])
    M.addField('node', data, 'dist')

    # 2. convert to field defined at element nodes
    M.convertField('dist', 'elemn', 'distn')

    # 3. convert to field constant over elements
    M.convertField('distn', 'elemc', 'distc')

    # 4. convert back to node field
    fld = M.getField('distc').convert('node')

    print(M.fieldReport())

    # draw the fields
    pf.viewport(0)
    pf.drawField(M.getField('dist'))
    pf.zoomAll()
    pf.viewport(1)
    pf.drawField(M.getField('distn'))
    pf.zoomAll()
    pf.viewport(2)
    pf.drawField(M.getField('distc'))
    pf.zoomAll()
    pf.viewport(3)
    pf.drawField(fld)
    pf.zoomAll()


if __name__ == '__draw__':
    run()

# End
