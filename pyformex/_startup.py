#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""pyFormex startup utilities

This module contains functionality needed during pyFormex startup.
"""
import sys
import os
import importlib.machinery

import pyformex as pf
from pyformex.path import Path

__all__ = ['version', 'fullversion', 'banner', 'printwrap',
           'FatalError', 'RequirementsError', 'ImplementationError',
           'pyformexdir', '_installtype', 'Path'
           ]

__revision__ = pf.__version__
__branch__ = "master"

# Intended Python and NumPy versions
# We support 3.8+ only, because we use self documenting f-strings
minimal_version = 0x03080000
target_version = 0x030B0000
numpy_version = ['>=1.16', '<2.0']

_license_ = (
    "pyFormex comes with ABSOLUTELY NO WARRANTY. This is free software, "
    "and you are welcome to redistribute it under the conditions of the "
    "GNU General Public License, version 3 or later. For details see the "
    "LICENSE file or the Help/License menu option in the GUI.")

def version():
    """Return a string with the pyFormex name and version"""
    return f"pyFormex {pf.__version__}"


def fullversion():
    """Return a string with the pyFormex name, version and revision"""
    if __revision__ == pf.__version__:
        return version()
    else:
        return f"{version()} ({__revision__}) DEVELOPMENT VERSION"

def banner():
    """Return the program banner"""
    return f"{pf.fullversion()}  (C) Benedict Verhegghe\n" + _license_ + '\n'


messages = []

# Fatal errors
class FatalError(Exception):
    """Raised on fatal or grave errors"""
    def __init__(self, msg):
        from inspect import cleandoc as dedent
        s = '\n' + '#' * 72
        s = '\n##  '.join([s] + dedent(msg).split('\n')) + s
        Exception.__init__(self, s)


class RequirementsError(FatalError):
    """Raised on missing or invalid requirements"""
    pass

class ImplementationError(FatalError):
    """Raised on clear implementation errors"""
    pass


#########  Check Python version #############

def major(v):
    """Return the major component of version"""
    return v >> 24


def minor(v):
    """Return the minor component of version"""
    return (v & 0x00FF0000) >> 16


def human_version(v):
    """Return the human readable string for version"""
    return f"{v>>24}.{(v & 0x00FF0000) >> 16}"


if sys.hexversion < minimal_version:
    # Older than minimal
    y = human_version(sys.hexversion)
    m = human_version(minimal_version)
    raise RequirementsError(f"""\
Your Python version is {y}, but pyFormex requires Python >= {m}.
We advice you to upgrade your Python version.
""")

if sys.hexversion & 0xFFFF0000 > target_version:
    # Major,minor newer than target:
    y = human_version(sys.hexversion)
    t = human_version(target_version)
    messages.append(
        f"Your Python version is {y}, but {version()} has only been tested\n"
        f"with Python <= {t}. We expect {version()} to run correctly with\n"
        f"your Python version, but if you encounter problems, please\n"
        f"contact the developers at http://pyformex.org."
    )

# TODO: the source tree stuff could be moved to a separate module
def run_cmd(cmd):
    """Run command"""
    import subprocess
    P = subprocess.Popen(
        str(cmd), stdout=subprocess.PIPE, shell=True, universal_newlines=True
    )
    out, err = P.communicate()
    if P.returncode:
        print(out)
        print(err)
    return P.returncode, out, err

def clean_source(pyformexdir):
    global messages
    if __debug__:
        print(f"Clean the source tree {pyformexdir}")
    source_clean = pyformexdir.parent / 'source_clean'
    if source_clean.exists():
        try:
            suffix = importlib.machinery.EXTENSION_SUFFIXES[0]
            cmd = f"{source_clean} --keep_modules '*{suffix}'"
            run_cmd(cmd)
        except Exception as e:
            messages.append(
                str(e) + f"\nError while executing {source_clean} was ignored")

def checkLibraries(libdir, libraries):
    """Check which extension modules need to be recompiled"""
    if __debug__:
        print("Check compiled libraries (.so)")
    import sysconfig
    ext = sysconfig.get_config_var('EXT_SUFFIX')
    if ext is None:
        ext = '.so'
    nok = []
    for lib in libraries:
        ver = libdir / 'version.h'
        src = libdir / lib + '.c'
        obj = libdir / lib + ext
        if not obj.exists() or obj.mtime < src.mtime or obj.mtime < ver.mtime:
            nok.append(lib)
    return nok

def checkRebuildLibraries(pyformexdir, libraries):
    global messages
    if __debug__:
        print("Make sure the compiled libraries are up-to-date")
    libdir = pyformexdir / 'lib'
    nok = checkLibraries(libdir, libraries)
    if nok:
        print("Libraries needing recompile:", nok)
        print("Rebuilding pyFormex libraries, please wait")
        cmd = f"cd {pyformexdir.parent}; PYTHON={sys.executable} make lib"
        run_cmd(cmd)
        nok = checkLibraries(libdir, libraries)
    if nok:
        # No success
        messages.append(
            f"Even after an attempt to recompile there remained some "
            f"out of date libraries in {libdir}: {nok}.\n"
            "You should probably exit pyFormex, fix the problem and "
            "then restart pyFormex.")


# Set the proper revision/branch when running from git sources
def set_revision_branch(pyformexdir):
    global messages, __revision__, __branch__
    if __debug__:
        print("Set git branch and revision")
    cmd = f"cd {pyformexdir} && git describe --always"
    sta, out, err = run_cmd(cmd)
    if sta == 0:
        __revision__ = out.split('\n')[0].strip()
    else:
        messages.append("Could not set revision")
    cmd = f"cd {pyformexdir} && git rev-parse --abbrev-ref HEAD"
    sta, out, err = run_cmd(cmd)
    if sta == 0:
        __branch__ = out.split('\n')[0]
    else:
        messages.append("Could not set branch name")

def detect_installtype():
    #### Detect install type ########

    # Install type.
    # This can have the following values:
    #     'U' : normal installation as user (from source or tarball)
    #           The builtin pyFormex removal is activated.
    #     'R' : normal installation as root (from source or tarball)
    #           The builtin pyFormex removal is activated.
    #     'G' : unreleased version running from GIT sources: no installation
    #           required. This is set if the directory containing the
    #           pyformex start script is a git repository. The builtin pyFormex
    #           removal is deactivated.
    #     'D' : Distribution package of a released version, official or not
    #           (e.g. Debian package). The distribution package tools should
    #           be used to install/uninstall. The builtin pyFormex removal is
    #           deactivated.
    #
    installtype = 'R'
    pyformexdir = Path(__file__).parent
    if (pyformexdir.parent / '.git').exists():
        installtype = 'G'
        clean_source(pyformexdir)
        checkRebuildLibraries(pyformexdir, ['misc_c', 'nurbs_c', 'clust_c'])
        set_revision_branch(pyformexdir)
    else:
        if os.getuid() != 0 and (pyformexdir / 'doc').is_writable_dir():
            installtype = 'U'
    return pyformexdir, installtype


def printwrap(text, **kargs):
    """Print a text wrapped to terminal size"""
    import textwrap
    if 'width' not in kargs:
        kargs['width'], _ = os.get_terminal_size()
    print(textwrap.fill(text, **kargs))


def initialize_numpy(version=None, printoptions={}, typelessdata=[]):
    from pyformex import software
    # Check required modules
    if version:
        software.Module.require('numpy', version)
    # if software.Module.check('numpy', '>=2.0'):
    #     print("WARNING! You are using NumPy 2.0, which is not fully supported yet")
    #     ans = input('Continue? Y/n ')
    #     if ans[:1].lower() == 'n':
    #         import sys
    #         sys.exit()
    import numpy
    if printoptions.get('legacy', None) == 'auto':
        if software.Module.check('numpy', '>=2.0'):
             legacy = '1.25'
        elif software.Module.check('numpy', '>=1.21'):
            legacy = '1.21'
        elif software.Module.check('numpy', '>=1.13'):
            legacy = '1.13'
        else:
            legacy = False
        printoptions['legacy'] = legacy
    if printoptions:
        numpy.set_printoptions(**printoptions)
    if typelessdata:
        if software.Module.check('numpy', '>=2.0'):
            from numpy._core import arrayprint
        else:
            from numpy.core import arrayprint
        for typ in typelessdata:
            arrayprint._typelessdata.append(getattr(numpy, typ))


pyformexdir, _installtype = detect_installtype()
if messages:
    print("** pyFormex startup message **\n")
    print('\n\n'.join(messages))
    print()
    messages = []


# # End
