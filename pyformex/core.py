# flake8: noqa
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Softwae Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Default imports in the scripting language.

This module defines which symbols are always imported when
you run a script, and thus constitute the core part
of the pyFormex scripting language.

All the definitions in this module are imported into the pyformex module
at the end of the pyFormex startup. The recommended way to use the core
is to use the short 'pf' alias::

  import pyformex as pf

and then prefix everyhing you need with a 'pf.'.

There is a similar :mod:`gui.guicore` module with the GUI functionality.

Examples
--------
>>> import pyformex as pf
>>> print(pf.version())
pyFormex 4...
>>> print(pf.Color('red'))
(1.0, 0.0, 0.0)

Warning
-------
This module is currently in transient state and subject to future changes.
"""
#print("IMPORT CORE")
from pyformex import arraytools
from pyformex import candy
from pyformex import color
from pyformex import coords
from pyformex import curve
from pyformex import fileread
from pyformex import filewrite
from pyformex import formex
from pyformex import geomtools
from pyformex import inertia
from pyformex import mesh
from pyformex import software
from pyformex import script
from pyformex import simple
from pyformex import utils
from pyformex.plugins import nurbs

at = arraytools
gt = geomtools

from pyformex.arraytools import Int, Float, DEG, RAD, sind, cosd, tand
from pyformex.color import Color, colorArray, palette
from pyformex.coords import Coords, bbox, bboxIntersection, align, pattern, origin
from pyformex.coordsys import CoordSys
from pyformex.curve import BezierSpline, PolyLine, Contour
from pyformex.elements import Elems
from pyformex.field import Field
from pyformex.fileread import readGeometry
from pyformex.filetools import (File, TempFile, TempDir, NameSequence, autoName,
                                getDocString, metadoc)
from pyformex.filetype import Filetype
from pyformex.filewrite import writeGeometry
from pyformex.formex import Formex, connect
from pyformex.geometry import Geometry
from pyformex.geomfile import GeometryFile
from pyformex.globalformat import floatformat, intformat
from pyformex._main import onExit, savePreferences
from pyformex.mesh import Mesh
from pyformex.multi import splitArgs, multitask
from pyformex.namespace import Namespace
from pyformex.path import Path, hsortkey, sortkey
cwd = Path.cwd
from pyformex.polygons import Polygons
from pyformex.project import Project
from pyformex.pzffile import PzfFile
from pyformex.olist import List
from pyformex.software import Version, Module, External, formatDict
from pyformex.script import chdir, mkdir, breakpt, exit, sleep
from pyformex.timing import Timing, Timings
from pyformex.trisurface import TriSurface, mergedSurface, fillBorder
from pyformex.utils import command
from pyformex.varray import Varray

from pyformex.plugins.nurbs import (NurbsCurve, globalInterpolationCurve,
                                    NurbsSurface)

# End
