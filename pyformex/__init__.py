#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""pyFormex

pyFormex is a tool for generating, manipulating and transforming large
geometrical models of 3D structures by sequences of mathematical transformations.

pyFormex is not a complete application for all 3D modelling needs, but rather a
programming framework to quickly develop specialized 3D applications.

pyFormex contains:

#. A well tested library of classes aimed at efficiently handling 3D geometry,
   especially triangulized surfaces, finite element meshes and curves.
#. A collection of low level support classes to build upon.
#. A Qt based library for fast and easy GUI development to provide interactive
   support interactive geometry handling.
#. A quality OpenGL rendering engine to display the geometry and computational
   results.
#. A vast set of examples demonstrating many of the possibilities.
#. A compiled acceleration library for some time critical functions.

pyFormex has been under development since 2004 and is still maintained by
the original author. It is distributed under the GNU General Public License
version 3 or later.
"""
import datetime
_start_time = datetime.datetime.now()
del datetime
import sys  # noqa: E402

__version__ = "4.0.dev1"
_copyright = 'Copyright (C) 2004-2024 Benedict Verhegghe'
_url = 'http://pyformex.org'

from pyformex._startup import *  # noqa: F403

# Placeholder for excutable, if pyformex: filled in by startup.py
_executable = Path(sys.executable)
if __debug__:
    print(f"INIT: {_executable=}")
    print(f"{__name__=}")

################ pyFormex global variables and functions ###############

# The GUI parts
_app_started = False
interactive = False
app = None  # the QApplication
GUI = None  # the GUI QMainWindow
canvas = None  # the OpenGL Drawing widget controlled by the running script
console = None  # alternate Python console

# Initialize some global variables used for communication between modules
from pyformex.debug import DEBUG, debugon, verbosity

# Detect special modes
_doctest = False     # running a doctest?
_pytest = 'pytest' in sys.modules    # importing from pytest?
_sphinx = 'sphinx' in sys.modules    # importing from sphinx
if _sphinx or _pytest:
    # Create options needed for loading some modules
    from pyformex.namespace import Namespace
    options = Namespace(debuglevel=DEBUG.NONE, gui=False, gl3=False,
                        verbose=0, uselib=_pytest)

####################################################################
##  Configuration ##

from pyformex.config import Config
# Create a config instance
cfg = Config({'pyformexdir': pyformexdir})  # noqa:  F405
# load the factory defaults
cfg.load(cfg['pyformexdir'] / 'pyformexrc')

# Set the current session configurations in pyformex module
prefcfg = None  # the preferenced configuration
refcfg = None  # the reference configuration
preffile = None  # the file where the preferenced configuration will be saved

# Import the pyFormex main function
from pyformex._main import main, onExit, savePreferences

def publicKeys(module=None, skip='_'):
    """List the public keys in the pyFormex core language"""
    if module is None:
        keys = globals().keys()
    else:
        keys = dir(module)
    return sorted([k for k in keys if not k.startswith(skip)])

def _public_vars(module=None, skip='_'):
    if module is None:
        _dict = globals()
    else:
        _dict = vars(module)
    return [item for item in _dict.items() if (
        not item[0].startswith(skip) or item[0] in _dict.get('_public_', []))]

# Import the public vars from a module into the pyFormex language
def _import_language(module):
    if __debug__:
        print(f"FINALIZE_STARTUP {publicKeys(module)=}")
    globals().update(_public_vars(module))

def _import_core():
    global _core_imported
    if '_core_imported' in globals():
        return
    from pyformex import core
    _import_language(core)


if __debug__:
    print("pyFormex global definitions:", publicKeys(skip='__'))
    print("pyFormex public definitions:", publicKeys())

# Remove symbols that were imported for local use
del Config


##################################################
## Protect the pyformex module attributes
############################################
import types
class Globals(types.ModuleType):
    def __setattr__(module, name, value):
        if __debug__:
            print(f"Setting global {name} ({type(value).__name__})")
        if name == 'PF':
            from pyformex.project import Project
            if not isinstance(value, Project):
                raise ValueError("PF should be a Project")
        globals()[name] = value


sys.modules[__name__].__class__ = Globals

if _sphinx or _pytest:
    print("IMPORT CORE")
    _import_core()

# End
