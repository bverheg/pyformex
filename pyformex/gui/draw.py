# flake8: noqa
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Legacy module for compatibility with pyFormex v3 scripts.

This module should not be used for new applications and older
applications should be adapted to remove their depencence on it.
"""
import pyformex as pf
if pf.options.doctest is None:
    pf.utils.warn('depr_gui_draw')
if pf.options.legacy:
    from pyformex import utils
    from pyformex.core import *
    from pyformex.script import *
    from pyformex.color import *
    from pyformex.coords import *
    from pyformex.gui.guiscript import *
    from pyformex.gui.widgets import _I, _G, _C, _T, Dialog
    from pyformex.gui.dialogs import FileDialog
    from pyformex.gui import views
    from pyformex.gui.toolbar import pick
    from pyformex.gui.timer import Timer
    globals().update(pf.color.PF_COLORS)

    #######################################################
    ## deprecated ##

    @utils.deprecated_by("drawText")
    def drawText3D(*args, **kargs):
        return drawText(*args, **kargs)

    @utils.deprecated_by('showHtml')  # release 3.1
    def showHTML(fn=None):
        return showHtml(fn, server=False)

    @utils.deprecated_by("pf.warning")
    def showWarning(text, actions=['OK'], **kargs):
        """Show a warning message and wait for user acknowledgement."""
        return showMessage(text, actions, 'warning', **kargs)

    @utils.deprecated_by("pf.error")
    def showError(text, actions=['OK'], **kargs):
        """Show an error message and wait for user acknowledgement."""
        return showMessage(text, actions, 'error', **kargs)

    warning = pf.warning
    error = pf.error

# End
