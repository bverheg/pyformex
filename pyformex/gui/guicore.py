# flake8: noqa
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Default imports in the GUI scripting language.

When pyFormex is started with GUI, the contents of this module is imported
in the main pyformex module. It thus becomes part of the pyFormex scripting
language and everyhing can be access easily by a single import::

  import pyformex as pf

"""
# make these public, despite the leading underscore
_public_ = ['_I', '_G', '_C', '_T', '_H']

from pyformex.gui import menu

# TODO: check what is in guiscript and should be imported here
from pyformex.gui.guiscript import *
from pyformex.gui.widgets import _I, _G, _C, _T, _H, Dialog, ImageView
from pyformex.gui.dialogs import *
from pyformex.gui.image import saveImage
from pyformex.gui.toolbar import pick
from pyformex.gui.timer import Timer
from pyformex.gui.colorscale import ColorScale, Palette
from pyformex.gui.menus import PluginMenu
from pyformex.gui.menus.Viewport import clearAll
from pyformex.gui.projectmgr import projectmanager as pmgr
from pyformex.opengl.decors import ColorLegend
from pyformex.opengl.fonttexture import FontTexture
from pyformex.plugins.imagearray import Image, image2array, array2image

# End
