#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""2D drawing menu

This pyFormex plugin menu provides some interactive 2D drawing functions.
While the drawing operations themselves are in 2D, they can be performed
on a plane with any orientation in space. The constructed geometry always
has 3D coordinates in the global cartesian coordinate system.
"""
import pyformex as pf
from pyformex.plugins import draw2d

################################## Menu #############################

def create_menu(before='help'):
    """Create the menu."""
    MenuData = [
        ("&Set grid", draw2d.create_grid),
        ("&Remove grid", draw2d.remove_grid),
        ("---", None),
        ("&Draw Points", draw2d.draw_points),
        ("&Draw Polyline", draw2d.draw_polyline),
        ("&Draw Curve", draw2d.draw_curve),
        ("&Draw Nurbs", draw2d.draw_nurbs),
        ("&Draw Circle", draw2d.draw_circle),
        ("---", None),
        ("&Split Curve", draw2d.split_curve),
        ("---", None),
    ]
    w = pf.gui.menu.Menu('Draw2d', items=MenuData, parent=pf.GUI.menu, before=before)
    return w

def on_reload():
    pf.setDrawOptions({'bbox': 'last'})


# End
