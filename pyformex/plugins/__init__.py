#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""A collection of extended functionality for pyFormex.

The :mod:`pyformex.plugins` subpackage contains a variety of modules
with extra functionality for pyFormex.
Here are some of the reasons why a module is the plugins subpackage and not
directly in the pyformex package:

- not important for pyFormex,
- under development,
- not considered stable,
- not documented well,
- a niche application,
- maintained by third parties,

Over time, some of these modules may be moved up into pyformex core, or
moved away to the attic, where they are no longer maintained. Meanwhile,
they are (as far as possible) maintained and tested like the core.
"""

# End
