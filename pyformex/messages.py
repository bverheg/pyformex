#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Filterable Warning Messages

"""
# flake8: noqa

_message_replace = []

_messages = {
    'camera_save': "The old format of saving the camera/canvas is deprecated. Use the menus or gui.guiscript.saveCamera and gui.guiscript.saveCanvas to save the camera/canvas in the new PZF format. The old format can still be loaded with gui.guiscript.loadCamera or gui.guiscript.loadCanvas, but it is recommended to immediately save it again in the new format.",
    'depr_asknewfilename': "The use of askNewFilename is deprecated. Use askFilename instead. It allows picking existing files as well as creating new files.",
    'depr_bezierspline_deriv': "The 'deriv' parameter of BezierSpline is deprecated. Use 'tangents' instead",
    'depr_cardinalspline': "CardinalSpline is deprecated. Use BezierSpline with ``curl=(1.-tension)/3.``",
    'depr_debug': "The use of ``pf.debug(message, debugflag)`` is deprecated. Instead use ``if pf.debugon(debugflag): print(message)``",
    'depr_gui_draw': "The use of the pyformex.gui.draw module is deprecated. Please use pyformex.gui.guicore or the pg alias instead.",
    'depr_mesh_getedges': "The Mesh methods getEdges, getElemEdges, getFaces and  getCells are deprecated. You can directly use the Mesh properties edges, elem_edges, faces (and the new elem_faces) and cells instead. They are computed when first time used and stored for later reuse.",
    'depr_naturalspline': "NaturalSpline is deprecated. For a closed curve or an open curve with endzerocurv=True, BezierSpline gives a good approximation. For an open curve with endzerocurv=False, a NurbsCurve obtained with nurbs.globalInterpolationCurve will do fine.",
    'depr_saveimage': "crop='canvas' is deprecated. Use crop='all' instead",
    'depr_trisurface_fixnormals': "The 'outward' parameter of TriSurface.fixNormals has been renamed to 'outwards'.",
    'depr_verbose': "The use of ``pf.verbose(verbose, message)`` is deprecated. Instead use ``if pf.verbosity(verbose): print(message)``",
    'dialog_store_save': "The 'store' facility of a Dialog is now read/write by default: accepted values are written back to the store. This provides for automatic persistence of data between invocations. If you prefer a read-only store, add the 'save=False' parameter",
    'error_widgets_enableitem': "Error in a dialog item enabler. This should not happen! Please file a bug report.",
    'test_message': "This is the warning shown to the user",
    'warn_Connectivity_inverse': "The default behavior of Connectivity.inverse has changed. It now returns a Varray by default. To get the old return value (a -1 padded array), you can either add the 'expand=True' parameter or (preferably) explicitely convert the returned Varray:  conn.inverse().toArray().",
    'warn_Interaction_changed': "Class fe_abq.Interaction has been changed, only allowed parameters are name, friction, surfacebehavior, surfaceinteraction, extra (see doc).",
    'warn_coords_match_changed': "The Coords.match function has changed. The argument 'clean' has been removed. The behavior with clean=True can be obtained with the hasMatch method",
    'warn_dolly_zoom_projection': "Dolly zoom moves the camera towards or away form the scene. In projection mode, this has no effect on the image. Use Mouse movement or the Mouse wheel to zoom in projection mode.",
    'warn_dualMesh': "TriSurface.dualMesh has changed. It now only returns the Mesh. The nodal areas can be computed easily afterward with arraytools.binsum(M.areas(), M.prop)",
    'warn_dxf_noparser': "I can not import .DXF format on your machine, because I can not find the required external program *dxfparser*. *dxfparser* comes with pyFormex, so this probably means that it just was not (properly) installed. The pyFormex install manual describes how to do it. If you are running pyFormex from Git sources and you can have root access, you can go to the directory `...pyformex/extra/dxfparser/` and follow the instructions there, or you can just try the **Install externals** menu option of the **Help** menu.",
    'warn_exit_all': "exit(all=True) is no longer supported.",
    'warn_fixnormals_default': "The default method of fixNormals has been changed from 'admesh' to 'internal'. This method does not include fuse/compact and guarantees that the node/element numbering is retained. You may want to do a fuse/compact prior to calling fixNormals. The 'admesh' method impies it.",
    'warn_formex_eltype': "Formex eltype currently needs to be a string!",
    'warn_inputcombo_onselect': "InputCombo: the use of onselect is deprecated. Use 'func' instead. It receives the InputItem as parameter.",
    'warn_mesh_extrude': "Mesh.extrude parameters have changed. The signature is now  extrude(div,dir,length) where length is the total extrusion length.",
    'warn_mesh_reduce_degenerate': "Mesh.reduceDegenerate has changed. It is now an alias for Mesh.splitDegenerate. See the docs.",
    'warn_mesh_smooth': "The Mesh.smooth and TrisSurface.smooth methods have changed. Consult the docs for the new API",
    'warn_nurbs_gic': "Your point set appears to contain double points. Currently I cannot handle that. I will skip the doubles and try to go ahead.",
    'warn_project_compression': "The contents of the file does not appear to be compressed.",
    'warn_radio_enabler': "A 'radio' type input item can currently not be used as an enabler for other input fields.",
    'warn_regular_grid': "The function simple.regularGrid has changed!. The points are now ordered first along the 0-axis, then along the 1-axis, etc... This is to be consistent with other grid numbering schemes in pyFormex. The old numbering scheme (first along the highest axis) can be obtained by passing the 'swapaxes=True' argument to the function.",
    'warn_viewport_linking': "Linking viewports is an experimental feature and is not fully functional yet.",
}


def getMessage(msg):
    """Return the real message corresponding with a given mnemonic.

    Parameters
    ----------
    msg: str
        A short mnemonic string pointing to a message in this module.

    Returns
    -------
    str
        The full message matching the *msg*, or *msg* itself if no such
        message was defined.

    Example
    -------
    >>> getMessage('test_message')
    'This is the warning shown to the user'
    >>> getMessage('another_message')
    'another_message'
    """
    msg = str(msg)  # allows for msg being a Warning
    msg = _messages.get(msg, msg)
    for tag, replacement in _message_replace:
        msg = msg.replace(tag, replacement)
    return msg

# End
