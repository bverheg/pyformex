..

..
  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
  SPDX-License-Identifier: GPL-3.0-or-later

  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
  pyFormex is a tool for generating, manipulating and transforming 3D
  geometrical models by sequences of mathematical operations.
  Home page: https://pyformex.org
  Project page: https://savannah.nongnu.org/projects/pyformex/
  Development: https://gitlab.com/bverheg/pyformex
  Distributed under the GNU General Public License version 3 or later.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/.


==============
pyformex-split
==============

-------------------
Split a GTS Surface
-------------------

:Author: Benedict Verhegghe <bverheg@gmail.com>. This manual page was written for the pyFormex project (and may be used by others).
:Date:   2024-04-03
:Copyright: GPL v3 or higher
:Version: 0.2
:Manual section: 1
:Manual group: text and X11 processing

SYNOPSIS
========

pyformex-split [OPTION] [FNAME] < file.gts

DESCRIPTION
===========

Splits the GTS file into connected and manifold components.
FNAME is the base name of the components created (default is 'component')

OPTIONS
=======

--verbose, -v        Print statistics about the surface.
--help, -h           Display this help and exit.


SEE ALSO
========

gtscheck


AUTHOR
======

The GTS library was written by Stéphane Popinet <popinet@users.sourceforge.net>.
The gtssmooth command is taken from the examples.
