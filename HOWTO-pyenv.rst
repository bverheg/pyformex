..

How to run pyFormex with a different Python version
---------------------------------------------------

.. warning: pyFormex only supports Python versions >= 3.9.0

In some cases you might want to run pyFormex with another Python
version than the standard Python3 version on your system:

- the standard version is not supported by pyFormex,
- you want to use a Python module requiring another Python version,
- you want to experiment with newer Python versions.

The easiest way to install non-system Python versions is through the use of
Pyenv (https://github.com/pyenv/pyenv#simple-python-version-management-pyenv).
Basically, it comes down to the following steps:

- install Pyenv
- install the prefered Python version
- run pyFormex with the specific version

All these can be done without root access.

Install Pyenv
.............
Follow the guide on the above link. I advise to use the git cloning method:
https://github.com/pyenv/pyenv?tab=readme-ov-file#basic-github-checkout
Make sure you also set up your environment:
https://github.com/pyenv/pyenv?tab=readme-ov-file#set-up-your-shell-environment-for-pyenv

Install Python versions
-----------------------
Next you can install one or more Python versions you want to use. It is as
simple as:

   pyenv install 3.12

Running pyFormex with a custom Python version
---------------------------------------------




.. End
