#!pyformex --nogui
print(pf.fullversion())

sphinxpath = pf.Path(__file__).parent
print(f"{sphinxpath=}")
template = sphinxpath / 'refman.template'
text = template.read_text()

for pkg in ('core', 'gui', 'gui.menus', 'fe', 'plugins', 'opengl', 'lib',
            'apps'):
    modules = pf.cmdtools.list_modules([pkg], sphinx=True)
    modules = ['   ref/' + m for m in modules]
    marker = f"%%{pkg.upper()}MODULES%%"
    print(marker)
    text = text.replace(marker, '\n'.join(modules))

refman = sphinxpath / 'refman.rst'
refman.write_text(text)
